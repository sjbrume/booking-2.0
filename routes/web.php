<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('pages.main');
Route::get('/home/{version?}', 'PageController@home')->name('pages.home');
Route::get('/about', 'PageController@about')->name('pages.about');
Route::get('/blog-grid', 'PageController@blogGrid')->name('pages.blogGrid');
Route::get('/blog-list', 'PageController@blogList')->name('pages.blogList');
Route::get('/blog-single', 'PageController@blogSingle')->name('pages.blogSingle');
Route::get('/coming-soon', 'PageController@comingSoon')->name('pages.comingSoon');
Route::get('/contact-us', 'PageController@contactUs')->name('pages.contactUs');
Route::get('/how-it-works', 'PageController@howItWorks')->name('pages.howItWorks');
Route::get('/legal-privacy', 'PageController@legalPrivacy')->name('pages.legalPrivacy');
Route::get('/property-grid', 'PageController@propertyGrid')->name('pages.propertyGrid');
Route::get('/property-grid-map', 'PageController@propertyGridMap')->name('pages.propertyGridMap');
Route::get('/property-list', 'PageController@propertyList')->name('pages.propertyList');
Route::get('/property-single', 'PageController@propertySigle')->name('pages.propertySingle');
