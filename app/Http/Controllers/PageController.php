<?php

namespace App\Http\Controllers;

class PageController extends Controller
{
    public function home(string $version = '')
    {
        $template = 'index';
        if (isset($version) && $version == 'v2') {
            $template = 'index_v2';
        }

        return view($template);
    }

    public function about()
    {
        return view('about');
    }

    public function blogGrid()
    {
        return view('blog_grid');
    }

    public function blogList()
    {
        return view('blog_list');
    }

    public function blogSingle()
    {
        return view('blog_single');
    }

    public function comingSoon()
    {
        return view('coming_soon');
    }

    public function contactUs()
    {
        return view('contact_us');
    }

    public function howItWorks()
    {
        return view('how_it_works');
    }

    public function legalPrivacy()
    {
        return view('legal_privacy');
    }

    public function propertyGrid()
    {
        return view('property_grid');
    }

    public function propertyGridMap()
    {
        return view('property_grid_map');
    }

    public function propertyList()
    {
        return view('property_list');
    }

    public function propertySingle()
    {
        return view('property_single');
    }
}
