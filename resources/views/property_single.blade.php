@extends('layouts.app')

@section('innerBanner')
    <!-- Inner Banner Start -->
    <div class="at-haslayout at-propertybannerholder">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="at-propertybannercontent">
                        <div class="at-propertyholder">
                            <figure class="at-propertyuserimg at-verifieduser">
                                <img src="/images/blog-single/user-img/img-01.jpg" alt="img description">
                                <i class="fa fa-shield-alt"></i>
                            </figure>
                            <div class="at-title">
                                <div class="at-tags">
                                    <a href="javascript:void(0);" class="at-tag">Featured</a>
                                    <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                </div>
                                <div class="at-username">
                                    <a href="javascript:void(0);">Bed &amp; Breakfast</a>
                                    <h2>Portland-Plush King Rooms</h2>
                                    <address><i class="fa fa-location-arrow"></i>14 Tottenham Court Road, New York</address>
                                </div>
                            </div>
                        </div>
                        <div class="at-rightarea">
                            <div class="at-singlerate">
                                <span><em>$640</em>/night</span>
                            </div>
                            <div class="at-featurerating">
                                <span class="at-stars"><span></span></span><em>14236 review</em>
                            </div>
                            <ul class="at-featureabout">
                                <li><a href="javascript:void(0);" class="at-like at-liked"><i class="far fa-heart"></i>Saved</a></li>
                                <li><a href="javascript:void(0);"><i class="fa fa-bug"></i>Report Property</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="at-propertysilder" class="at-propertysilder owl-carousel">
            <div class="item">
                <div class="at-propertysilder-img at-propertysilder-mtr">
                    <figure>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/93nuAUG7iGw">
                            <img src="/images/featured-img/icons/360.png" alt="img description" class="at-360-img">
                            <img src="/images/property-single/img-01.jpg" alt="img description">
                        </a>
                    </figure>
                    <figure>
                        <a href="/images/property-single/img-02.jpg" data-rel="prettyPhoto[gallery]">
                            <img src="/images/property-single/img-02.jpg" alt="img description">
                        </a>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8" >
                            <img src="/images/property-single/img-03.jpg" alt="img description">
                            <span class="at-video-icon"><i class="fab fa-youtube"></i></span>
                        </a>
                    </figure>
                </div>
            </div>
            <div class="item">
                <div class="at-propertysilder-img at-propertysilder-mtr2">
                    <figure>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/93nuAUG7iGw">
                            <img src="/images/featured-img/icons/360.png" alt="img description" class="at-360-img">
                            <img src="/images/property-single/img-01.jpg" alt="img description">
                        </a>
                    </figure>
                    <figure>
                        <a href="/images/property-single/img-02.jpg" data-rel="prettyPhoto[gallery]">
                            <img src="/images/property-single/img-02.jpg" alt="img description">
                        </a>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8" >
                            <img src="/images/property-single/img-03.jpg" alt="img description">
                            <span class="at-video-icon"><i class="fab fa-youtube"></i></span>
                        </a>
                    </figure>
                </div>
            </div>
            <div class="item">
                <div class="at-propertysilder-img at-propertysilder-mtr">
                    <figure>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/93nuAUG7iGw">
                            <img src="/images/featured-img/icons/360.png" alt="img description" class="at-360-img">
                            <img src="/images/property-single/img-01.jpg" alt="img description">
                        </a>
                    </figure>
                    <figure>
                        <a href="/images/property-single/img-02.jpg" data-rel="prettyPhoto[gallery]">
                            <img src="/images/property-single/img-02.jpg" alt="img description">
                        </a>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8" >
                            <img src="/images/property-single/img-03.jpg" alt="img description">
                            <span class="at-video-icon"><i class="fab fa-youtube"></i></span>
                        </a>
                    </figure>
                </div>
            </div>
            <div class="item">
                <div class="at-propertysilder-img at-propertysilder-mtr2">
                    <figure>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/93nuAUG7iGw">
                            <img src="/images/featured-img/icons/360.png" alt="img description" class="at-360-img">
                            <img src="/images/property-single/img-01.jpg" alt="img description">
                        </a>
                    </figure>
                    <figure>
                        <a href="/images/property-single/img-02.jpg" data-rel="prettyPhoto[gallery]">
                            <img src="/images/property-single/img-02.jpg" alt="img description">
                        </a>
                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8" >
                            <img src="/images/property-single/img-03.jpg" alt="img description">
                            <span class="at-video-icon"><i class="fab fa-youtube"></i></span>
                        </a>
                    </figure>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Slider End -->
@endsection

@section('content')
    <!-- Two Columns Start -->
    <div class="at-haslayout at-main-section at-propertysingle-mt">
        <div class="container">
            <div class="row">
                <div id="at-twocolumns" class="at-twocolumns at-haslayout">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 float-right">
                        <aside id="at-sidebar" class="at-sidebar float-left mt-md-0">
                            <div class="at-sideholder">
                                <a href="javascript:void(0);" id="at-closesidebar" class="at-closesidebar"><i class="ti-close"></i></a>
                                <div class="at-sidescrollbar">
                                    <div class="at-widgets-holder at-textfee-holder">
                                        <div class="at-widgets-title at-title-textfee" id="headingone" data-toggle="collapse" data-target="#collapseone" aria-expanded="true" aria-controls="collapseone" role="heading">
                                            <h2>$2528<span>/for 03 nights</span></h2>
                                        </div>
                                        <div class="at-widgets-content collapse show" id="collapseone" aria-labelledby="headingone">
                                            <ul class="at-taxesfees">
                                                <li><span>$640 x 3 nights <em>$1920<i class="far fa-question-circle toltip-content" data-tipso="Plus Member"></i></em></span></li>
                                                <li><span>Car Rental <em>$222<i class="far fa-question-circle toltip-content" data-tipso="Plus Member"></i></em></span></li>
                                                <li><span>Fresh Groceries <em>$16<i class="far fa-question-circle toltip-content" data-tipso="Plus Member"></i></em></span></li>
                                                <li><span>Medical Representative <em>$50<i class="far fa-question-circle toltip-content" data-tipso="Plus Member"></i></em></span></li>
                                                <li class="at-textfee"><span>Taxes &amp; Fees<i class="far fa-question-circle toltip-content" data-tipso="Plus Member"></i><em>$320</em></span></li>
                                                <li class="at-toteltextfee"><span>Total<em>$2528</em></span></li>
                                            </ul>
                                        </div>
                                        <div class="at-booking-holder">
                                            <form class="at-formtheme at-formbanner">
                                                <fieldset class="at-datetime">
                                                    <legend class="at-formtitle">When To Check In?</legend>
                                                    <div class="form-group">
                                                        <div class="at-selectdate-holder">
                                                            <div class="at-select">
                                                                <label>Check-In:</label>
                                                                <input type="text" id="at-startdate" class="form-control" placeholder="date">
                                                            </div>
                                                            <div class="at-select">
                                                                <label>Check-Out:</label>
                                                                <input type="text" id="at-enddate" class="form-control" placeholder="date">
                                                            </div>
                                                            <a href="javascript:void(0);" class="at-calendarbtn"><i class="ti-calendar"></i></a>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="at-guestsform">
                                                    <legend class="at-formtitle">Guests</legend>
                                                    <div class="form-group">
                                                        <ul class="at-guestsinfo">
                                                            <li>
                                                                <div class="at-gueststitle">
                                                                    <span>Adults</span>
                                                                </div>
                                                                <div class="at-guests-radioholder">

                                                                    <div class="at-dropdown">
                                                                        <span><em class="selected-search-type">01 </em><i class="ti-angle-down"></i></span>
                                                                    </div>
                                                                    <div class="at-radioholder">
                                                                            <span class="at-radio">
                                                                                <input id="at-adults1" data-title="01" type="radio" name="adults" value="adults" checked="">
                                                                                <label for="at-adults1">01</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults2" data-title="02" type="radio" name="adults" value="adults2">
                                                                                <label for="at-adults2">02</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults3" data-title="03" type="radio" name="adults" value="adults3">
                                                                                <label for="at-adults3">03</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults4" data-title="04" type="radio" name="adults" value="adults4">
                                                                                <label for="at-adults4">04</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults5" data-title="05" type="radio" name="adults" value="adults5">
                                                                                <label for="at-adults5">05</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults6" data-title="06" type="radio" name="adults" value="adults6">
                                                                                <label for="at-adults6">06</label>
                                                                            </span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="at-gueststitle">
                                                                    <span>Children <em>(Ages 2–12)</em></span>
                                                                </div>
                                                                <div class="at-guests-radioholder">
                                                                    <div class="at-dropdown">
                                                                        <span><em class="selected-search-type">03 </em><i class="ti-angle-down"></i></span>
                                                                    </div>
                                                                    <div class="at-radioholder">
                                                                            <span class="at-radio">
                                                                                <input id="at-adultsv1" data-title="01" type="radio" name="adults" value="adultsv1">
                                                                                <label for="at-adultsv1">01</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adultsv2" data-title="02" type="radio" name="adults" value="adultsv2">
                                                                                <label for="at-adultsv2">02</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adultsv3" data-title="03" type="radio" name="adults" value="adultsv3" checked="">
                                                                                <label for="at-adultsv3">03</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adultsv4" data-title="04" type="radio" name="adults" value="adultsv4">
                                                                                <label for="at-adultsv4">04</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adultsv5" data-title="05" type="radio" name="adults" value="adultsv5">
                                                                                <label for="at-adultsv5">05</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adultsv6" data-title="06" type="radio" name="adults" value="adultsv6">
                                                                                <label for="at-adultsv6">06</label>
                                                                            </span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="at-gueststitle">
                                                                    <span>Infants <em>(Under 2)</em></span>
                                                                </div>
                                                                <div class="at-guests-radioholder">
                                                                    <div class="at-dropdown at-dropdownonclick2">
                                                                        <span><em class="selected-search-type">02 </em><i class="ti-angle-down"></i></span>
                                                                    </div>
                                                                    <div class="at-radioholder">
                                                                            <span class="at-radio">
                                                                                <input id="at-adultsv1b" data-title="01" type="radio" name="adultsb" value="adultsb">
                                                                                <label for="at-adultsv1b">01</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adultsv2b" data-title="02" type="radio" name="adultsb" value="adults2b" checked="">
                                                                                <label for="at-adultsv2b">02</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults3b" data-title="03" type="radio" name="adultsb" value="adults3b">
                                                                                <label for="at-adults3b">03</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults4b" data-title="04" type="radio" name="adultsb" value="adults4b">
                                                                                <label for="at-adults4b">04</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults5b" data-title="05" type="radio" name="adultsb" value="adults5b">
                                                                                <label for="at-adults5b">05</label>
                                                                            </span>
                                                                        <span class="at-radio">
                                                                                <input id="at-adults6b" data-title="06" type="radio" name="adultsb" value="adults6b">
                                                                                <label for="at-adults6b">06</label>
                                                                            </span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <legend class="at-formtitle">Add-on Services</legend>
                                                    <div id="at-services-checkbox" class="at-services-checkbox">
                                                            <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-transport" type="checkbox" name="apartment1" checked>
                                                                <label for="at-transport">Airport Transport</label>
                                                            </span>
                                                        <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-rental" type="checkbox" name="condo1">
                                                                <label for="at-rental">Car Rental</label>
                                                            </span>
                                                        <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-groceries" type="checkbox" name="multi1">
                                                                <label for="at-groceries">Fresh Groceries</label>
                                                            </span>
                                                        <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-driver" type="checkbox" name="single1">
                                                                <label for="at-driver">Driver</label>
                                                            </span>
                                                        <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-medical" type="checkbox" name="farm1">
                                                                <label for="at-medical">Medical Representative</label>
                                                            </span>
                                                        <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-loft1" type="checkbox" name="loft1" checked>
                                                                <label for="at-loft1">Loft</label>
                                                            </span>
                                                        <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-villa1" type="checkbox" name="villa1">
                                                                <label for="at-villa1">Villa</label>
                                                            </span>
                                                        <span class="at-checkbox at-checkboxvtwo">
                                                                <input id="at-townhouse1" type="checkbox" name="townhouse1" checked>
                                                                <label for="at-townhouse1">Townhouse</label>
                                                            </span>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <div class="at-btnarea">
                                                        <a href="javascript:void(0)" class="at-btn at-btnactive">Book Now</a>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>About Host</h2>
                                        </div>
                                        <div class="at-widgets-content at-authorholder at-authorholdertwo">
                                            <figure class="at-authorimg at-verifieduser">
                                                <img src="/images/blog-single/user-img/img-07.jpg" alt="img description">
                                                <i class="fa fa-shield-alt"></i>
                                            </figure>
                                            <div class="at-authordetails">
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <h3>Bhagatveer Singh</h3>
                                                <span>Languages can speak <i class="far fa-question-circle toltip-content" data-tipso="Plus Member"></i></span>
                                            </div>
                                            <div class="at-btnarea">
                                                <a href="javascript:void(0);" class="at-btn">View All My Ads</a>
                                                <a href="javascript:void(0);" class="at-btncontact">Contact Direct To Host</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Share Property</h2>
                                        </div>
                                        <div class="at-widgets-content at-widgets-mt at-sharingicons">
                                            <ul class="at-socialicons at-socialiconsbg">
                                                <li class="at-facebook">
                                                    <a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a>
                                                </li>
                                                <li class="at-twitter">
                                                    <a href="javascript:void(0);"><i class="fab fa-twitter"></i></a>
                                                </li>
                                                <li class="at-facebook-messenger">
                                                    <a href="javascript:void(0);"><i class="fab fa-facebook-messenger"></i></a>
                                                </li>
                                                <li class="at-linkedin">
                                                    <a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                                <li class="at-whatsapp">
                                                    <a href="javascript:void(0);"><i class="fab fa-whatsapp"></i></a>
                                                </li>
                                                <li class="at-viber">
                                                    <a href="javascript:void(0);"><i class="fab fa-viber"></i></a>
                                                </li>
                                                <li class="at-googleplus">
                                                    <a href="javascript:void(0);"><i class="fab fa-google-plus-g"></i></a>
                                                </li>
                                                <li class="at-instagram">
                                                    <a href="javascript:void(0);"><i class="fab fa-instagram"></i></a>
                                                </li>
                                                <li class="at-code">
                                                    <a href="javascript:void(0);"><i class="fa fa-code"></i></a>
                                                </li>
                                                <li class="at-share">
                                                    <a href="javascript:void(0);"><i class="fa fa-clone"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="at-adholder">
                                        <figure class="at-adimg">
                                            <a href="javascript:void(0);">
                                                <img src="/images/ad-img.jpg" alt="img description">
                                            </a>
                                            <figcaption><span>Advertisement  300px X 250px</span></figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-8 float-left">
                        <div class="at-gridlist-option at-option-mt">
                            <a href="javascript:void(0);" id="at-btnopenclose" class="at-btnopenclose"><i class="ti-settings"></i></a>
                        </div>
                        <div class="at-propertylinkdetails at-haslayout">
                            <ul class="at-propertylink">
                                <li><a href="#at-about">About</a></li>
                                <li><a href="#at-amenetiesproperty">Ameneties Others</a></li>
                                <li><a href="#at-locationsproperty">Nearby</a></li>
                                <li><a href="#at-availability">Availability</a></li>
                                <li><a href="#at-termspolicy-holder">Terms &amp; Rules</a></li>
                                <li><a href="#at-comments">Reviews</a></li>
                                <li><a href="javascript:void(0);"><i class="ti-more-alt"></i></a></li>
                            </ul>
                            <div id="at-about" class="at-propertydetails at-aboutproperty">
                                <div class="at-propertytitle">
                                    <h3>About Property</h3>
                                </div>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nrud exercitation ullamco laboris nisi ute aliquip ex ea commodo consequat duis auete irure dolor in reprehenderit in voluptate velit.</p>
                                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id estae laborume Sed ut perspiciatis unde omnis iste natus error sitame voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta suntanes explicoe nemo enim ipsam voluptatem officia deserunt mollit anim.</p>
                                </div>
                            </div>
                            <div class="at-propertydetails at-detailsproperty">
                                <div class="at-propertytitle">
                                    <h3>Property Details</h3>
                                </div>
                                <ul class="at-detailslisting">
                                    <li><h4>Accomodation</h4><span>04 Guests</span></li>
                                    <li><h4>Bedrooms</h4><span>02</span></li>
                                    <li><h4>Bathrooms</h4><span>02</span></li>
                                    <li><h4>Dimension</h4><span>1200 Sq Ft</span></li>
                                    <li><h4>Type</h4><span>Seperate Room</span></li>
                                    <li><h4>Populor Nearby</h4><span>Yes</span></li>
                                    <li><h4>Check-In Start @</h4><span>09:00 am</span></li>
                                </ul>
                            </div>
                            <div id="at-amenetiesproperty" class="at-propertydetails at-amenetiesproperty">
                                <div class="at-propertytitle">
                                    <h3>Ameneties</h3>
                                </div>
                                <ul id="at-amenetieslisting" class="at-amenetieslisting">
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-01.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Consectetur adipisicing elit</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-02.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Reprehenderit in voluptate velit</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-03.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Eiusmod tempoer incididunt</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-04.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Excepteur sint occaecat cupidatat</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-05.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Labore et dolore magna aliqua</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-06.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Proident sunt in culpa qui</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-07.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Veniam quis nrud</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-08.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Officia deserunt mollit animidestae</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-09.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Exercitation ullamco laboris</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-10.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Laborume sed ut perspiciatis</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-01.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Consectetur adipisicing elit</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-02.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Reprehenderit in voluptate velit</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-03.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Eiusmod tempoer incididunt</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-amenetiesicon">
                                            <i><img src="/images/icons/img-04.jpg" alt="img description"></i>
                                        </div>
                                        <div class="at-amenetiescontent">
                                            <span>Excepteur sint occaecat cupidatat</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div id="at-locationsproperty" class="at-propertydetails at-locationsproperty">
                                <div class="at-propertytitle">
                                    <h3>Nearby Locations</h3>
                                </div>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation ullamco.</p>
                                </div>
                                <div id="at-nearbylocations-holder" class="at-nearbylocations-holder">
                                    <div class="at-nearbylocations">
                                        <div class="at-title">
                                            <h4><i class="ti-shopping-cart"></i>Shop/ Shopping Mall</h4>
                                        </div>
                                        <ul class="at-locationsinfo">
                                            <li>
                                                <span>Barelona Dream Port &amp; Shoppoing Mall <em>(0.02 mi)</em></span>
                                                <span>05 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Graceline Shoppoing Mall <em>(0.06 mi)</em></span>
                                                <span>10 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Asian Food Court <em>(0.08 mi)</em></span>
                                                <span>12 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Limelight Shake &amp; Drink <em>(0.09 mi)</em></span>
                                                <span>15 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="at-nearbylocations">
                                        <div class="at-title">
                                            <h4><i class="ti-face-smile"></i>Fun &amp; Entertainment</h4>
                                        </div>
                                        <ul class="at-locationsinfo">
                                            <li>
                                                <span>Bright Land Toy &amp; Gift Shop <em>(0.05 mi)</em></span>
                                                <span>05 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Sport Gear &amp; Play Land <em>(0.09 mi)</em></span>
                                                <span>10 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="at-nearbylocations">
                                        <div class="at-title">
                                            <h4><i class="ti-music-alt"></i>Night Life &amp; Pubs</h4>
                                        </div>
                                        <ul class="at-locationsinfo">
                                            <li>
                                                <span>Mia’s Pub &amp; Restaurant <em>(0.05 mi)</em></span>
                                                <span>05 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Real Men Bar <em>(0.09 mi)</em></span>
                                                <span>10 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="at-nearbylocations">
                                        <div class="at-title">
                                            <h4><i class="ti-heart-broken"></i>Hospitals &amp; Clinics</h4>
                                        </div>
                                        <ul class="at-locationsinfo">
                                            <li>
                                                <span>Barelona Dream Port &amp; Shoppoing Mall</span>
                                                <span>05 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Graceline Shoppoing Mall</span>
                                                <span>10 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Asian Food Court</span>
                                                <span>12 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Limelight Shake &amp; Drink</span>
                                                <span>15 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="at-nearbylocations">
                                        <div class="at-title">
                                            <h4><i class="ti-shopping-cart"></i>Shop/ Shopping Mall</h4>
                                        </div>
                                        <ul class="at-locationsinfo">
                                            <li>
                                                <span>Barelona Dream Port &amp; Shoppoing Mall <em>(0.02 mi)</em></span>
                                                <span>05 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                            <li>
                                                <span>Graceline Shoppoing Mall <em>(0.06 mi)</em></span>
                                                <span>10 min walk <a href="javascript:void(0);">(Directions)</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="at-availability" class="at-propertydetails at-availability">
                                <div class="at-propertytitle">
                                    <h3>Availability</h3>
                                </div>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation ullamco.</p>
                                </div>
                                <div class="at-availability-holder">
                                    <div id="at-calendar-slider" class="at-calendar-slider owl-carousel">
                                        <div class="at-calendar-two item">
                                            <div id="at-calendar1" class="at-calendar at-disabled"></div>
                                        </div>
                                        <div class="at-calendar-two item">
                                            <div id="at-calendar2" class="at-calendar at-disabled"></div>
                                        </div>
                                        <div class="at-calendar-two item">
                                            <div id="at-calendar3" class="at-calendar at-disabled"></div>
                                        </div>
                                    </div>
                                    <div class="at-availability-status">
                                        <span class="at-available">Available</span>
                                        <span class="at-occupied">Occupied</span>
                                        <span class="at-closed">Closed</span>
                                    </div>
                                </div>
                            </div>
                            <div id="at-termspolicy-holder" class="at-termspolicy-holder at-haslayout">
                                <div class="at-termspolicy">
                                    <figure class="at-termspolicy-img">
                                        <img src="/images/property-single/team-img.jpg" alt="img description">
                                    </figure>
                                    <div class="at-termspolicy-content">
                                        <div class="at-title">
                                            <h3>Terms &amp; Policy</h3>
                                        </div>
                                        <div class="at-description">
                                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore etdolore magna aliqua enim adminim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat aute irure dolor in reprehenderit ina voluptate velit esse cillum fugiat nulla pariatur.</p>
                                        </div>
                                        <div class="at-btnarea">
                                            <a href="javascript:void(0);" class="at-btn at-btnactive">Read All Policies</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="at-comments" class="at-comments at-commentstwo">
                                <div class="at-title">
                                    <h2>256 Comments</h2>
                                </div>
                                <ul>
                                    <li>
                                        <div class="at-comment">
                                            <figure class="at-commentimg"><img src="/images/blog-single/user-img/img-03.jpg" alt="img description"></figure>
                                            <div class="at-commentdetails">
                                                <div class="at-title">
                                                    <a href=" javascript:void(0);">Hunter Lamoureaux</a>
                                                    <h3>It was as great as expected. Amazing</h3>
                                                    <span>Jun 27, 2019</span>
                                                </div>
                                                <div class="at-reviewrating">
                                                    <div class="at-rightarea">
                                                        <span class="at-stars"><span></span></span><em>Average</em>
                                                    </div>
                                                </div>
                                                <div class="at-description">
                                                    <p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-comment">
                                            <figure class="at-commentimg"><img src="/images/blog-single/user-img/img-05.jpg" alt="img description"></figure>
                                            <div class="at-commentdetails">
                                                <div class="at-title">
                                                    <a href=" javascript:void(0);">Angela Julie</a>
                                                    <h3>Great post of the day, Appreciate</h3>
                                                    <span>Jun 27, 2019</span>
                                                </div>
                                                <div class="at-reviewrating">
                                                    <div class="at-rightarea">
                                                        <span class="at-stars"><span></span></span><em>Average</em>
                                                    </div>
                                                </div>
                                                <div class="at-description">
                                                    <p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-comment">
                                            <figure class="at-commentimg"><img src="/images/blog-single/user-img/img-06.jpg" alt="img description"></figure>
                                            <div class="at-commentdetails">
                                                <div class="at-title">
                                                    <a href=" javascript:void(0);">Angela Julie</a>
                                                    <h3>Great post of the day, Appreciate</h3>
                                                    <span>Jun 27, 2019</span>
                                                </div>
                                                <div class="at-reviewrating">
                                                    <div class="at-rightarea">
                                                        <span class="at-stars"><span></span></span><em>Average</em>
                                                    </div>
                                                </div>
                                                <div class="at-description">
                                                    <p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="at-btnarea">
                                        <a href="javascript:void(0);" class="at-btn">View All</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Two Columns End -->
@endsection
