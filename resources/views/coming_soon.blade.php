@extends('layouts.app')

@section('content')
    <!-- Coming Soon Section Start -->
    <div class="at-haslayout at-comingsoonholder">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-5">
                    <div class="at-comingsoon">
                        <div class="at-logocs">
                            <strong><img src="/images/logo.png" alt="img description">
                                <img src="/images/logo2.png" alt="img description">
                            </strong>
                        </div>
                        <div class="at-comingsoon-holder">
                            <div class="at-comingsoon-info">
                                <div class="at-title">
                                    <h2><span>Our Legends Working Very Hard</span>We’re Launching Very Soon!</h2>
                                </div>
                                <div class="at-description">
                                    <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco.</p>
                                </div>
                            </div>
                            <ul id="at-comming-sooncounter" class="at-comming-sooncounter">
                                <li id="days" class="timer_box days">
                                    <h1>244</h1><p>Days</p>
                                </li>
                                <li id="hours" class="timer_box hours">
                                    <h1>10</h1><p>Hours</p>
                                </li>
                                <li id="minutes" class="timer_box minutes">
                                    <h1>54</h1><p>Minutes</p>
                                </li>
                                <li id="seconds" class="timer_box seconds">
                                    <h1>56</h1><p>Seconds</p>
                                </li>
                            </ul>
                            <div class="at-newslettercs">
                                <h4>Signup For Newsletter</h4>
                                <div class="form-group">
                                    <input type="text" name="Your Email ID" class="form-control" placeholder="Your Email ID">
                                    <a href="javascript:void(0);"><i class="ti-email"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="at-footercs">
                            <span>Copyrights © 2019 by <em>Tenanto</em>. All Rights Reserved.</span>
                            <ul class="at-socialicons float-right">
                                <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-7">
                    <div class="row">
                        <div class="at-comingsoonimg">
                            <figure id="at-comingsoonimgslider" class="at-comingsoonimgslider owl-carousel">
                                <img src="/images/comingsoon/img-01.jpg" alt="img description">
                                <img src="/images/comingsoon/img-02.jpg" alt="img description">
                            </figure>
                            <div class="at-logoicon"><img src="/images/comingsoon/img-01.png" alt="img description"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Coming Soon Section End -->
@endsection

@section('scripts')
    <script src="js/counter.js"></script>
@endsection
