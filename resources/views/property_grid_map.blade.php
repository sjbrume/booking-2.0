@extends('layouts.app')

@section('innerBanner')
    <!-- Map Start -->
    <div class="at-haslayout at-gridmap-holder">
        <div id="at-locationmap" class="at-locationmap"></div>
        <div class="at-innerbanner-holder at-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="at-innerbanner-search">
                            <form class="at-formtheme at-form-advancedsearch">
                                <fieldset>
                                    <div class="form-group">
                                        <div class="at-select">
                                            <select>
                                                <option value="">Where You Want To Stay</option>
                                                <option value="twitter">China</option>
                                                <option value="linkedin">France</option>
                                                <option value="rss">Germany</option>
                                                <option value="vimeo">Italy</option>
                                                <option value="tumblr">Japan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group at-checkin-holder at-datecheck">
                                        <input type="text" id="at-startdate" class="form-control" placeholder="in">
                                    </div>
                                    <div class="form-group at-checkout-holder at-datecheck">
                                        <input type="text" id="at-enddate" class="form-control" placeholder="out">
                                    </div>
                                    <div class="at-btnarea">
                                        <a href="javascript:void(0);" class="at-btn at-btnactive">Search Now</a>
                                    </div>
                                    <a href="javascript:void(0);" class="at-docsearch"><span class="at-advanceicon"><i></i> <i></i> <i></i></span></a>
                                </fieldset>
                            </form>
                            <div class="at-advancedsearch-holder">
                                <div class="at-advancedsearch">
                                    <div class="at-title">
                                        <h3>Number Of Guests:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-selectholder">
                                            <div class="at-select">
                                                <select>
                                                    <option value="" hidden selected>No. of Adults</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                            <div class="at-select">
                                                <select>
                                                    <option value="" hidden selected>No. of Children (Ages 2–12)</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                            <div class="at-select">
                                                <select>
                                                    <option value="" hidden selected>No. of Infants (Under 2)</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch">
                                    <div class="at-title">
                                        <h3>Room Type:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-room-radioholder at-room-radioholdervtwo">
												<span class="at-radio">
													<input id="at-private" type="radio" name="shared" checked>
													<label for="at-private">
														<img src="/images/radio-imgs/img-07.jpg" alt="img">
														<span><em>Private Room</em> (58,1250)</span>
													</label>
												</span>
                                            <span class="at-radio">
													<input id="at-shared" type="radio" name="shared">
													<label for="at-shared">
														<img src="/images/radio-imgs/img-08.jpg" alt="img">
														<span><em>Shared Room</em> (31,5245)</span>
													</label>
												</span>
                                            <span class="at-radio">
													<input id="at-entire" type="radio" name="shared">
													<label for="at-entire">
														<img src="/images/radio-imgs/img-09.jpg" alt="img">
														<span><em>Entire Place</em> (22,4523)</span>
													</label>
												</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch">
                                    <div class="at-title">
                                        <h3>Accomodation Type:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-widget-checkbox">
												<span class="at-checkbox at-checkboxvtwo">
													<input id="at-apartment1" type="checkbox" name="apartment1" checked>
													<label for="at-apartment1">Apartment</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-condo1" type="checkbox" name="condo1">
													<label for="at-condo1">Condo</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-multi1" type="checkbox" name="multi1">
													<label for="at-multi1">Multi Family Space</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-single1" type="checkbox" name="single1">
													<label for="at-single1">Single Family Space</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-farm1" type="checkbox" name="farm1">
													<label for="at-farm1">Farm</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-loft1" type="checkbox" name="loft1" checked>
													<label for="at-loft1">Loft</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-villa1" type="checkbox" name="villa1">
													<label for="at-villa1">Villa</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-townhouse1" type="checkbox" name="townhouse1" checked>
													<label for="at-townhouse1">Townhouse</label>
												</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch">
                                    <div class="at-title">
                                        <h3>Area Size:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-areasizebox">
                                            <input type="text"  id="at-areaangeafttwo" class="at-areaangeafttwo" readonly>
                                        </div>
                                        <div id="at-arearangeslidertwo" class="at-arearangeslidertwo at-themerangeslider"></div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch at-searchradio">
                                    <div class="at-title">
                                        <h3>Number Of Bedrooms:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-guests-radioholder">
												<span class="at-radio">
													<input id="at-adults1v" type="radio" name="adultsv" value="adults">
													<label for="at-adults1v">01</label>
												</span>
                                            <span class="at-radio">
													<input id="at-adults2v" type="radio" name="adultsv" value="adults2" checked="">
													<label for="at-adults2v">02</label>
												</span>
                                            <span class="at-radio">
													<input id="at-adults3v" type="radio" name="adultsv" value="adults3">
													<label for="at-adults3v">03</label>
												</span>
                                            <div class="at-dropdown">
                                                <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                            </div>
                                            <div class="at-radioholder">
													<span class="at-radio">
														<input id="at-adults4v" data-title="04" type="radio" name="adultsv" value="adults4">
														<label for="at-adults4v">04</label>
													</span>
                                                <span class="at-radio">
														<input id="at-adults5v" data-title="05" type="radio" name="adultsv" value="adults5">
														<label for="at-adults5v">05</label>
													</span>
                                                <span class="at-radio">
														<input id="at-adults6v" data-title="06" type="radio" name="adultsv" value="adults6">
														<label for="at-adults6v">06</label>
													</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch at-searchradio">
                                    <div class="at-title">
                                        <h3>Number Of Bathrooms:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-guests-radioholder">
												<span class="at-radio">
													<input id="at-adultsv1a" type="radio" name="adultsa" value="adultsv1">
													<label for="at-adultsv1a">01</label>
												</span>
                                            <span class="at-radio">
													<input id="at-adultsv2a" type="radio" name="adultsa" value="adultsv2">
													<label for="at-adultsv2a">02</label>
												</span>
                                            <span class="at-radio">
													<input id="at-adultsv3a" type="radio" name="adultsa" value="adultsv3" checked="">
													<label for="at-adultsv3a">03</label>
												</span>
                                            <div class="at-dropdown">
                                                <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                            </div>
                                            <div class="at-radioholder">
													<span class="at-radio">
														<input id="at-adultsv4a" data-title="04" type="radio" name="adultsa" value="adultsv4">
														<label for="at-adultsv4a">04</label>
													</span>
                                                <span class="at-radio">
														<input id="at-adultsv5a" data-title="05" type="radio" name="adultsa" value="adultsv5">
														<label for="at-adultsv5a">05</label>
													</span>
                                                <span class="at-radio">
														<input id="at-adultsv6a" data-title="06" type="radio" name="adultsa" value="adultsv6">
														<label for="at-adultsv6a">06</label>
													</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch">
                                    <div class="at-title">
                                        <h3>Price Range:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-areasizebox">
                                            <input type="text"  id="at-rangeamounttwo" readonly>
                                        </div>
                                        <div id="at-rangeslidertwo" class="at-rangeslidertwo at-themerangeslider"></div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch">
                                    <div class="at-title">
                                        <h3>Amenities:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-widget-checkbox">
												<span class="at-checkbox at-checkboxvtwo">
													<input id="at-wifitwo" type="checkbox" name="wifi" checked>
													<label for="at-wifitwo">Wi-Fi</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-cabletwo" type="checkbox" name="cable">
													<label for="at-cabletwo">TV Cable</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-swimingtwo" type="checkbox" name="swiming">
													<label for="at-swimingtwo">Swiming Pool</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-saunatwo" type="checkbox" name="sauna">
													<label for="at-saunatwo">Sauna</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-laundrytwo" type="checkbox" name="laundry">
													<label for="at-laundrytwo">Laundry</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-conditioningtwo" type="checkbox" name="conditioning">
													<label for="at-conditioningtwo">Air Conditioning</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-barbecuetwo" type="checkbox" name="loft" checked>
													<label for="at-barbecuetwo">Barbecue Area</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-dishwashertwo" type="checkbox" name="dishwasher">
													<label for="at-dishwashertwo">Dishwasher</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-gymtwo" type="checkbox" name="gym" checked>
													<label for="at-gymtwo">Gym</label>
												</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="at-advancedsearch">
                                    <div class="at-title">
                                        <h3>Facilities:</h3>
                                    </div>
                                    <div class="at-searchcontent">
                                        <div class="at-widget-checkbox">
												<span class="at-checkbox at-checkboxvtwo">
													<input id="at-parkingtwo" type="checkbox" name="parking" checked>
													<label for="at-parkingtwo">Free Parking</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-marketstwo" type="checkbox" name="markets">
													<label for="at-marketstwo">Markets</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-playgroundtwo" type="checkbox" name="playground">
													<label for="at-playgroundtwo">Playground</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-beachsidetwo" type="checkbox" name="beachside">
													<label for="at-beachsidetwo">Beachside</label>
												</span>
                                            <span class="at-checkbox at-checkboxvtwo">
													<input id="at-pharmacytwo" type="checkbox" name="pharmacy" checked>
													<label for="at-pharmacytwo">Pharmacy</label>
												</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="at-filtersoptions">
                                    <div class="at-searchcontent">
                                        <p>Consectetur adipisicing elit sed eiusmod tempor incididunt</p>
                                        <div class="at-rightarea">
                                            <a href="javascript:void(0);" class="at-btn">Apply Filters</a>
                                            <a href="javascript:void(0);" class="at-resetf">Reset All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Map Start -->
@endsection

@section('content')
    <div class="at-haslayout at-main-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 float-left">
                        <!-- Properties grid Start -->
                        <div class="at-showresult-holder">
                            <div class="at-resulttitle">
                                <span>12,560 matches found for: <strong>“House” In “Manchester”</strong></span>
                            </div>
                            <div class="at-rightarea">
                                <div class="at-select">
                                    <select>
                                        <option value="Sort By:" hidden>Sort By:</option>
                                        <option value="Sort By:">Sort By Date</option>
                                        <option value="Sort By:">Sort By Featured</option>
                                    </select>
                                </div>
                                <div class="at-gridlist-option">
                                    <a href="javascript:void(0);"><i class="ti-layout-grid2"></i></a>
                                    <a href="javascript:void(0);"><i class="ti-view-list"></i></a>
                                    <a href="javascript:void(0);" class="at-linkactive"><i class="ti-location-pin"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="at-properties-grid at-properties-map at-haslayout">
                            <div class="row">
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/img-01.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Apartment</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Mountain Retreat Room <span>$240 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/img-04.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>46 Morningside Road, London, UK</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                <a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8" class="at-video-icon"><i class="fab fa-youtube"></i></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/img-01.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Bed &amp; Breakfast</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Balsam Fir Bungalow Suite <span>$120 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/img-05.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>14 Tottenham Court Road, New York</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/img-03.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <img src="/images/featured-img/icons/360.png" alt="img description" class="at-360-img">
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-03.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>11 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-01.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>01 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/img-06.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>3 Edgar Buildings, Austrailia</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/grid/img-01.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>12 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Save<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-03.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>11 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-01.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>01 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/user-img/img-04.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>3 Edgar Buildings, Austrailia</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/grid/img-02.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>12 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Save<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-03.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>11 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-01.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>01 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/user-img/img-05.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>3 Edgar Buildings, Austrailia</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/grid/img-03.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>12 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Save<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-03.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>11 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-01.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>01 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/user-img/img-06.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>3 Edgar Buildings, Austrailia</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/grid/img-04.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>12 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Save<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-03.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>11 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-01.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>01 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/user-img/img-07.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>3 Edgar Buildings, Austrailia</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/grid/img-05.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>12 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Save<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-03.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>11 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-01.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>01 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/user-img/img-08.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>3 Edgar Buildings, Austrailia</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-xl-4">
                                    <div class="at-featured-holder">
                                        <div class="at-featuredslider owl-carousel">
                                            <figure class="item">
                                                <a href="javascript:void(0);"><img src="/images/featured-img/grid/img-02.jpg" alt="img description" class="item"></a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>12 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Save<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-03.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>11 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <figure class="item">
                                                <a href="javascript:void(0);">
                                                    <img src="/images/featured-img/img-01.jpg" alt="img description" class="item">
                                                </a>
                                                <figcaption>
                                                    <div class="at-slider-details">
                                                        <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                        <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                        <span class="at-photolayer"><i class="fas fa-layer-group"></i>01 Photos</span>
                                                        <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="at-featured-content">
                                            <div class="at-featured-head">
                                                <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                                <div class="at-featured-title">
                                                    <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                                    <div class="at-userimg at-verifieduser">
                                                        <img src="/images/featured-img/user-img/img-05.jpg" alt="img description">
                                                        <i class="fa fa-shield-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="at-featurerating">
                                                    <span class="at-stars"><span></span></span><em>14236 review</em>
                                                </div>
                                                <ul class="at-room-featured">
                                                    <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                                    <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                                </ul>
                                            </div>
                                            <div class="at-featured-footer">
                                                <address>3 Edgar Buildings, Austrailia</address>
                                                <div class="at-share-holder">
                                                    <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                                    <div class="at-share-option">
                                                        <span>Share:</span>
                                                        <ul class="at-socialicons">
                                                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                        <a href="javascript:void(0);">Report</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav class="at-pagination">
                                <ul>
                                    <li class="at-prevpage"><a href="javascrip:void(0);"><i class="ti-angle-left"></i></a></li>
                                    <li class="at-active"><a href="javascrip:void(0);">1</a></li>
                                    <li><a href="javascrip:void(0);">2</a></li>
                                    <li><a href="javascrip:void(0);">3</a></li>
                                    <li><a href="javascrip:void(0);">4</a></li>
                                    <li><a href="javascrip:void(0);">...</a></li>
                                    <li><a href="javascrip:void(0);">50</a></li>
                                    <li class="at-nextpage"><a href="javascrip:void(0);"><i class="ti-angle-right"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Properties grid End -->
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ3q6w3hiHe_MIbB1Jy31bGOwL8LYlwJw"></script>
    <script src="js/gmap3.js"></script>
    <script>
        var center = [37.772323, -122.214897];
        $('#at-locationmap')
            .gmap3({
                center: center,
                zoom: 13,
                mapTypeId : google.maps.MapTypeId.ROADMAP
            })
            .marker({
                position: center,
                icon: 'https://maps.google.com/mapfiles/marker_green.png'
            });
    </script>
@endsection
