@extends('layouts.app')

@section('innerBanner')
    <!-- Inner Banner Start -->
    <div class="at-haslayout at-innerbannerholder">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-12">
                    <div class="at-innerbannercontent">
                        <div class="at-title"><h2>We’re Happy To Serve You</h2></div>
                        <ol class="at-breadcrumb">
                            <li><a href="{{ route('pages.main') }}">Main</a></li>
                            <li>Contact</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Slider End -->
@endsection

@section('content')
    <!-- Contact Form Start -->
    <section class="at-haslayout at-main-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 push-md-0 col-lg-10 push-lg-1 col-xl-8 push-xl-2">
                    <div class="at-sectionhead">
                        <div class="at-sectiontitle">
                            <h2>Get In Touch With Us</h2>
                            <span>We Offer 24/7 Support</span>
                        </div>
                        <div class="at-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisiut aliquip</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 push-md-0 col-lg-12 col-xl-10 push-xl-1">
                    <form class="at-formtheme at-formcontactus">
                        <fieldset>
                            <div class="form-group form-group-half">
                                <input type="text" name="First Name" class="form-control" placeholder="First Name">
                            </div>
                            <div class="form-group form-group-half">
                                <input type="text" name="Last Name" class="form-control" placeholder="Last Name">
                            </div>
                            <div class="form-group form-group-half">
                                <input type="email" name="Your Email" class="form-control" placeholder="Your Email">
                            </div>
                            <div class="form-group form-group-half">
                                <input type="text" name="Subject" class="form-control" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Message" required></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="at-btn">Send Now</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Form End -->
    <div class="at-haslayout">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="at-contactus-details">
                        <figure class="at-contactbg">
                            <img src="/images/contactus/bg-img.png" alt="img description">
                        </figure>
                        <div class="at-contactinfo">
                            <img src="/images/contactus/img-01.jpg" alt="img description">
                            <span>Talk To Us</span>
                            <h3>(+1) 2345 67 89 00</h3>
                        </div>
                        <div class="at-contactinfo">
                            <img src="/images/contactus/img-02.jpg" alt="img description">
                            <span>Send Us Email</span>
                            <h3>info@yourdomain.com</h3>
                        </div>
                        <div class="at-contactinfo">
                            <img src="/images/contactus/img-03.jpg" alt="img description">
                            <span>Our Open Location</span>
                            <h3>44-46 Brightside Road, Scotland, EH10 4BF</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Map Start -->
    <div class="at-haslayout at-contactmap-holder">
        <div id="at-locationmap" class="at-locationmap"></div>
    </div>
    <!-- Map Start -->
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ3q6w3hiHe_MIbB1Jy31bGOwL8LYlwJw"></script>
    <script>
        var center = [37.772323, -122.214897];
        $('#at-locationmap')
            .gmap3({
                center: center,
                zoom: 13,
                mapTypeId : google.maps.MapTypeId.ROADMAP
            })
            .marker({
                position: center,
                icon: 'https://maps.google.com/mapfiles/marker_green.png'
            });
    </script>
@endsection
