@extends('layouts.app')

@section('innerBanner')
    <!-- Inner Banner Start -->
    <div class="at-haslayout at-innerbannerholder">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-12">
                    <div class="at-innerbannercontent">
                        <div class="at-title"><h2>Get Latest Updates &amp; Tips</h2></div>
                        <ol class="at-breadcrumb">
                            <li><a href="{{ route('pages.main') }}">Main</a></li>
                            <li>Blogs</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Slider End -->
@endsection

@section('content')
    <!-- Two Columns Start -->
    <div class="at-haslayout at-main-section">
        <div class="container">
            <div class="row">
                <div id="at-twocolumns" class="at-twocolumns at-haslayout">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 float-right">
                        <aside id="at-sidebar" class="at-sidebar float-left mt-md-0">
                            <div class="at-sideholder">
                                <a href="javascript:void(0);" id="at-closesidebar" class="at-closesidebar"><i class="ti-close"></i></a>
                                <div class="at-sidescrollbar">
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Search</h2>
                                        </div>
                                        <div class="at-widgets-content at-formsearch-holder">
                                            <form class="at-formtheme at-formsearch">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <input type="text" name="Search" class="form-control" placeholder="Search Category">
                                                        <a href="javascrip:void(0);" class="at-searchgbtn"><i class="ti-search"></i></a>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Archives</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <div class="at-sidebarinfo at-archives-holder">
                                                <a href="javascript:void(0);">2019</a>
                                                <a href="javascript:void(0);">2016</a>
                                                <a href="javascript:void(0);">2018</a>
                                                <a href="javascript:void(0);">2015</a>
                                                <a href="javascript:void(0);">2017</a>
                                                <a href="javascript:void(0);">2014</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Top Commented</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <ul class="at-toprated">
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-08.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>10 Things All Rooms Should Ha...<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-09.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Margarito Beverage</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Best Days To Book Hotel For Tour<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-10.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Choose Any Place To Travel Any...<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-11.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Mountain Retreat Room<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-12.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3><a href="javascript:void(0);"> Portland-Plush KING Room</a><span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="at-adholder">
                                        <figure class="at-adimg">
                                            <a href="javascript:void(0);">
                                                <img src="/images/ad-img.jpg" alt="img description">
                                            </a>
                                            <figcaption><span>Advertisement  300px X 250px</span></figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-8 float-left">
                        <div class="at-howitwork-holder at-haslayout">
                            <div class="at-gridlist-option at-option-mt">
                                <a href="javascript:void(0);" id="at-btnopenclose" class="at-btnopenclose"><i class="ti-settings"></i></a>
                            </div>
                            <div class="at-howitwork">
                                <div class="row">
                                    <div class="col-12 col-md-7 col-lg-12 col-xl-7">
                                        <div class="at-howitworkinfo">
                                            <div class="at-howitwork-counter">
                                                <span>01.</span>
                                            </div>
                                            <div class="at-description">
                                                <h3>Search Accomodation</h3>
                                                <p>Lorem ipsum dolor sit amet, at a consectetur adipisicing elit, sed do eiusmod  incididunt ut labore et dolore magna aliqua enim ad minim veniam quistan ullamco.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5 col-lg-12 col-xl-5 ">
                                        <figure class="at-howitworkimg"><img src="/images/work/img-01.jpg" alt="img description"></figure>
                                    </div>
                                </div>
                            </div>
                            <div class="at-howitwork">
                                <div class="row">
                                    <div class="col-12 col-md-7 col-lg-12 col-xl-7">
                                        <div class="at-howitworkinfo">
                                            <div class="at-howitwork-counter">
                                                <span>02.</span>
                                            </div>
                                            <div class="at-description">
                                                <h3>Search Accomodation</h3>
                                                <p>Lorem ipsum dolor sit amet, at a consectetur adipisicing elit, sed do eiusmod  incididunt ut labore et dolore magna aliqua enim ad minim veniam quistan ullamco.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5 col-lg-12 col-xl-5 ">
                                        <figure class="at-howitworkimg"><img src="/images/work/img-02.jpg" alt="img description"></figure>
                                    </div>
                                </div>
                            </div>
                            <div class="at-howitwork">
                                <div class="row">
                                    <div class="col-12 col-md-7 col-lg-12 col-xl-7">
                                        <div class="at-howitworkinfo">
                                            <div class="at-howitwork-counter">
                                                <span>03.</span>
                                            </div>
                                            <div class="at-description">
                                                <h3>Search Accomodation</h3>
                                                <p>Lorem ipsum dolor sit amet, at a consectetur adipisicing elit, sed do eiusmod  incididunt ut labore et dolore magna aliqua enim ad minim veniam quistan ullamco.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5 col-lg-12 col-xl-5 ">
                                        <figure class="at-howitworkimg"><img src="/images/work/img-03.jpg" alt="img description"></figure>
                                    </div>
                                </div>
                            </div>
                            <div class="at-howitwork">
                                <div class="row">
                                    <div class="col-12 col-md-7 col-lg-12 col-xl-7">
                                        <div class="at-howitworkinfo">
                                            <div class="at-howitwork-counter">
                                                <span>04.</span>
                                            </div>
                                            <div class="at-description">
                                                <h3>Search Accomodation</h3>
                                                <p>Lorem ipsum dolor sit amet, at a consectetur adipisicing elit, sed do eiusmod  incididunt ut labore et dolore magna aliqua enim ad minim veniam quistan ullamco.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5 col-lg-12 col-xl-5 ">
                                        <figure class="at-howitworkimg"><img src="/images/work/img-04.jpg" alt="img description"></figure>
                                    </div>
                                </div>
                            </div>
                            <div class="at-howitwork-details">
                                <div class="at-title">
                                    <h2>What You're Waiting For?</h2>
                                </div>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua enim ad minim veniam.</p>
                                </div>
                                <div class="at-btnarea"><a href="javascript:void(0);" class="at-btn">Start Searching</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Two Columns End -->
@endsection
