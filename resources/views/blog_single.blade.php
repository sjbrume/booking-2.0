@extends('layouts.app')

@section('innerBanner')
    <!-- Inner Banner Start -->
    <div class="at-haslayout at-blogbannerholder">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="at-blogbannercontent">
                        <figure class="at-bloguserimg">
                            <img src="/images/blog-single/user-img/img-01.jpg" alt="img description">
                        </figure>
                        <div class="at-title">
                            <div class="at-username">
                                <a href="javascript:void(0);">Marivel Rosenberry</a>
                                <h2>Choose Any Place To Travel Anywhere</h2>
                            </div>
                            <ul class="at-userreport">
                                <li><span>Jun 27, 2019</span></li>
                                <li><span>15,063 Viewed</span></li>
                                <li><span>164 Comments</span></li>
                                <li><a href="javascript:void(0);" class="at-report">Report now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Slider End -->
@endsection

@section('content')
    <!-- Two Columns Start -->
    <div class="at-haslayout at-main-section">
        <div class="container">
            <div class="row">
                <div id="at-twocolumns" class="at-twocolumns at-haslayout">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 float-right">
                        <aside id="at-sidebar" class="at-sidebar float-left mt-md-0">
                            <div class="at-sideholder">
                                <a href="javascript:void(0);" id="at-closesidebar" class="at-closesidebar"><i class="ti-close"></i></a>
                                <div class="at-sidescrollbar">
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>About Author</h2>
                                        </div>
                                        <div class="at-widgets-content at-authorholder">
                                            <figure class="at-authorimg"><img src="/images/blog-single/user-img/img-02.jpg" alt="img description"></figure>
                                            <div class="at-authordetails">
                                                <h3>Rebecca Arnold</h3>
                                                <span>Since: Jun 27, 2019</span>
                                                <ul class="at-socialicons">
                                                    <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                    <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                    <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="at-btnarea">
                                                <a href="javascript:void(0);" class="at-btn">View All My Posts</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Follow Author</h2>
                                        </div>
                                        <div class="at-widgets-content at-widgets-mt at-authorfollow">
                                            <ul class="at-socialicons at-socialiconsbg">
                                                <li class="at-facebook">
                                                    <a href="javascript:void(0);"><i class="fab fa-facebook-f"></i><span><em>13.5k</em>Likes</span></a>
                                                </li>
                                                <li class="at-youtube">
                                                    <a href="javascript:void(0);"><i class="fab fa-youtube"></i><span><em>250k</em>Subscribe</span></a>
                                                </li>
                                                <li class="at-googleplus">
                                                    <a href="javascript:void(0);"><i class="fab fa-google-plus-g"></i><span><em>32k</em>Followers</span></a>
                                                </li>
                                                <li class="at-instagram">
                                                    <a href="javascript:void(0);"><i class="fab fa-instagram"></i><span><em>22.6k</em>Likes</span></a>
                                                </li>
                                                <li class="at-vimeo">
                                                    <a href="javascript:void(0);"><i class="fab fa-vimeo-v"></i><span><em>66k</em> Followers</span></a>
                                                </li>
                                                <li class="at-tumblr">
                                                    <a href="javascript:void(0);"><i class="fab fa-tumblr"></i><span><em>16k</em> Followers</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Archives</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <div class="at-sidebarinfo at-archives-holder">
                                                <a href="javascript:void(0);">2019</a>
                                                <a href="javascript:void(0);">2016</a>
                                                <a href="javascript:void(0);">2018</a>
                                                <a href="javascript:void(0);">2015</a>
                                                <a href="javascript:void(0);">2017</a>
                                                <a href="javascript:void(0);">2014</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Top Commented</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <ul class="at-toprated">
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-08.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>10 Things All Rooms Should Ha...<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-09.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Margarito Beverage</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Best Days To Book Hotel For Tour<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-10.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Choose Any Place To Travel Any...<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-11.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Mountain Retreat Room<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-12.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3><a href="javascript:void(0);"> Portland-Plush KING Room</a><span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="at-adholder">
                                        <figure class="at-adimg">
                                            <a href="javascript:void(0);">
                                                <img src="/images/ad-img.jpg" alt="img description">
                                            </a>
                                            <figcaption><span>Advertisement  300px X 250px</span></figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-8 float-left">
                        <div class="at-blogsingle">
                            <div class="at-gridlist-option at-option-mt">
                                <a href="javascript:void(0);" id="at-btnopenclose" class="at-btnopenclose"><i class="ti-settings"></i></a>
                            </div>
                            <div class="at-blogsingle-description">
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nrud exercitation ullamco laboris nisi ute aliquip ex ea commodo consequat duis auete irure dolor in reprehenderit in voluptate velit.</p>
                                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id estae laborume Sed ut perspiciatis unde omnis iste natus error sitame voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta suntanes explicoe nemo enim ipsam voluptatem officia deserunt mollit anim.</p>
                                    <blockquote class="at-blockquote">
                                        <q>Deserunt mollit anim id estae laborume Sed ut perspiciatis unde omnis iste natus error sitae volutatem accusantium doloremue laudantium, totam rem aeriam.</q>
                                        <span class="at-quote-icon"><i class="fa fa-quote-right"></i></span>
                                    </blockquote>
                                    <p>Sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nrudism exercitation ullamco laboris nisi ute aliquip ex ea commodo consequat duis.</p>
                                    <p>Sunt in culpa qui officia deserunt mollit anim id estae laborume Sed ut perspiciatis unde omnis iste natus error sitame voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta suntanes explicoe nemo.</p>
                                    <figure class="at-blogsingle-video"><a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8"><img src="/images/blog-single/img-01.jpg" alt="img description"></a></figure>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nrud exercitation ullamco laboris nisi ute aliquip ex ea commodo consequat duis auete irure dolor in reprehenderit in voluptate velit.</p>
                                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id estae laborume Sed ut perspiciatis unde omnis iste natus error sitame voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta suntanes explicoe nemo enim ipsam voluptatem officia deserunt mollit anim.</p>
                                </div>
                            </div>
                            <div class="at-tagsshare-holder">
                                <ul class="at-widgettag">
                                    <li><a href="javascript:void(0);">Classified</a></li>
                                    <li><a href="javascript:void(0);">DIY</a></li>
                                    <li><a href="javascript:void(0);">Vacations</a></li>
                                    <li><a href="javascript:void(0);">Travel</a></li>
                                    <li><a href="javascript:void(0);">Tourism</a></li>
                                </ul>
                                <div class="at-tagsshare">
                                    <a href="javascript:void(0);">Share:<i id="at-shareooption" class="fa fa-share"></i> </a>
                                    <ul class="at-socialicons">
                                        <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                        <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                        <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                        <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="at-comments" class="at-comments">
                                <div class="at-title">
                                    <h2>256 Comments</h2>
                                </div>
                                <ul>
                                    <li>
                                        <div class="at-comment">
                                            <figure class="at-commentimg"><img src="/images/blog-single/user-img/img-03.jpg"><figcaption><a href="javascript:void(0);">Reply<i class="fa fa-reply"></i></a></figcaption></figure>
                                            <div class="at-commentdetails">
                                                <div class="at-title">
                                                    <a href=" javascript:void(0);">Angela Julie</a>
                                                    <h3>Great post of the day, Appreciate</h3>
                                                    <span>Jun 27, 2019</span>
                                                </div>
                                                <div class="at-description">
                                                    <p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="children">
                                            <li>
                                                <div class="at-comment">
                                                    <figure class="at-commentimg"><img src="/images/blog-single/user-img/img-04.jpg"><figcaption><a href="javascript:void(0);">Reply<i class="fa fa-reply"></i></a></figcaption></figure>
                                                    <div class="at-commentdetails">
                                                        <div class="at-title">
                                                            <a href=" javascript:void(0);">Angela Julie</a>
                                                            <h3>Great post of the day, Appreciate</h3>
                                                            <span>Jun 27, 2019</span>
                                                        </div>
                                                        <div class="at-description">
                                                            <p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="at-comment">
                                            <figure class="at-commentimg"><img src="/images/blog-single/user-img/img-05.jpg"><figcaption><a href="javascript:void(0);">Reply<i class="fa fa-reply"></i></a></figcaption></figure>
                                            <div class="at-commentdetails">
                                                <div class="at-title">
                                                    <a href=" javascript:void(0);">Angela Julie</a>
                                                    <h3>Great post of the day, Appreciate</h3>
                                                    <span>Jun 27, 2019</span>
                                                </div>
                                                <div class="at-description">
                                                    <p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="at-comment">
                                            <figure class="at-commentimg"><img src="/images/blog-single/user-img/img-06.jpg"><figcaption><a href="javascript:void(0);">Reply<i class="fa fa-reply"></i></a></figcaption></figure>
                                            <div class="at-commentdetails">
                                                <div class="at-title">
                                                    <a href=" javascript:void(0);">Angela Julie</a>
                                                    <h3>Great post of the day, Appreciate</h3>
                                                    <span>Jun 27, 2019</span>
                                                </div>
                                                <div class="at-description">
                                                    <p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore dolore magna aliqua ut enim ad minim veniam, quis nrud exercitation.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="at-btnarea">
                                        <a href="javascript:void(0);" class="at-btn">View All</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="at-replaybox">
                                <div class="at-title">
                                    <h3>Leave Your Comment</h3>
                                </div>
                                <form class="at-formtheme at-formcomment">
                                    <fieldset>
                                        <div class="form-group form-group-half">
                                            <input type="text" name="First Name" class="form-control" placeholder="First Name" required="">
                                        </div>
                                        <div class="form-group form-group-half">
                                            <input type="text" name="Last Name" class="form-control" placeholder="Last Name" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="Subject" class="form-control" placeholder="Subject" required="">
                                        </div>
                                        <div class="form-group">
                                            <textarea name="message" class="form-control" placeholder="Your Comment" required></textarea>
                                        </div>
                                        <div class="form-group at-btnarea">
                                            <button type="submit" class="at-btn at-btnactive">Submit</button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Two Columns End -->
@endsection
