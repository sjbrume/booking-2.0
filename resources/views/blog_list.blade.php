@extends('layouts.app')

@section('innerBanner')
    <!-- Inner Banner Start -->
    <div class="at-haslayout at-innerbannerholder">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-12">
                    <div class="at-innerbannercontent">
                        <div class="at-title"><h2>Get Latest Updates &amp; Tips</h2></div>
                        <ol class="at-breadcrumb">
                            <li><a href="{{ route('pages.main') }}">Main</a></li>
                            <li>Blogs</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Slider End -->
@endsection

@section('content')
    <!-- Two Columns Start -->
    <div class="at-haslayout at-main-section">
        <div class="container">
            <div class="row">
                <div id="at-twocolumns" class="at-twocolumns at-haslayout">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 float-right">
                        <aside id="at-sidebar" class="at-sidebar float-left mt-md-0">
                            <div class="at-sideholder">
                                <a href="javascript:void(0);" id="at-closesidebar" class="at-closesidebar"><i class="ti-close"></i></a>
                                <div class="at-sidescrollbar">
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Search</h2>
                                        </div>
                                        <div class="at-widgets-content at-formsearch-holder">
                                            <form class="at-formtheme at-formsearch">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <input type="text" name="Search" class="form-control" placeholder="Search Category">
                                                        <a href="javascrip:void(0);" class="at-searchgbtn"><i class="ti-search"></i></a>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Categories</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <div class="at-sidebarinfo at-categories-holder">
                                                <a href="javascript:void(0);">All</a>
                                                <a href="javascript:void(0);">WordPress</a>
                                                <a href="javascript:void(0);">HTML Template</a>
                                                <a href="javascript:void(0);">Static Design</a>
                                                <a href="javascript:void(0);">Joomla</a>
                                                <a href="javascript:void(0);">CMS Framework</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Archives</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <div class="at-sidebarinfo at-archives-holder">
                                                <a href="javascript:void(0);">2019</a>
                                                <a href="javascript:void(0);">2016</a>
                                                <a href="javascript:void(0);">2018</a>
                                                <a href="javascript:void(0);">2015</a>
                                                <a href="javascript:void(0);">2017</a>
                                                <a href="javascript:void(0);">2014</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Top Commented</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <ul class="at-toprated">
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-08.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>10 Things All Rooms Should Ha...<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-09.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Margarito Beverage</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Best Days To Book Hotel For Tour<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-10.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Choose Any Place To Travel Any...<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-11.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3>Mountain Retreat Room<span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="at-toprated-content">
                                                    <figure><img src="/images/featured-img/listing/img-12.jpg" alt="img description"></figure>
                                                    <div class="at-topratedlisting">
                                                        <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                                        <div class="at-topratedtitle">
                                                            <h3><a href="javascript:void(0);"> Portland-Plush KING Room</a><span><em>Jun 27, 2019</em></span></h3>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="at-adholder">
                                        <figure class="at-adimg">
                                            <a href="javascript:void(0);">
                                                <img src="/images/ad-img.jpg" alt="img description">
                                            </a>
                                            <figcaption><span>Advertisement  300px X 250px</span></figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-8 float-left">
                        <!-- Properties List Start -->
                        <div class="at-showresult-holder">
                            <div class="at-resulttitle">
                                <h3>Our Latest Blogs</h3>
                            </div>
                            <div class="at-rightarea">
                                <div class="at-select">
                                    <select>
                                        <option value="Sort By:" hidden>Sort By:</option>
                                        <option value="Sort By:">Sort By Date</option>
                                        <option value="Sort By:">Sort By Featured</option>
                                    </select>
                                </div>
                                <div class="at-gridlist-option">
                                    <a href="javascript:void(0);"><i class="ti-layout-grid2"></i></a>
                                    <a href="javascript:void(0);" class="at-linkactive"><i class="ti-view-list"></i></a>
                                    <a href="javascript:void(0);" id="at-btnopenclose" class="at-btnopenclose"><i class="ti-settings"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="at-blog-list at-haslayout">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-01.jpg" alt="img description">
                                            <figcaption><a href="javascript:void(0);" class="at-tag">Featured</a></figcaption>
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                            <div class="at-title">
                                                <h4>10 Things All Rooms Should Have</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-02.jpg" alt="img description">
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Margarito Beverage</a> </div>
                                            <div class="at-title">
                                                <h4>Best Days To Book Hotel For Tour</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-03.jpg" alt="img description">
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                            <div class="at-title">
                                                <h4>Choose Any Place To Travel Anywhere</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-04.jpg" alt="img description">
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                            <div class="at-title">
                                                <h4>Choose Any Place To Travel Anywhere</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-05.jpg" alt="img description">
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                            <div class="at-title">
                                                <h4>Choose Any Place To Travel Anywhere</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-06.jpg" alt="img description">
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                            <div class="at-title">
                                                <h4>Choose Any Place To Travel Anywhere</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-02.jpg" alt="img description">
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Margarito Beverage</a> </div>
                                            <div class="at-title">
                                                <h4>Best Days To Book Hotel For Tour</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="at-article">
                                        <figure class="at-articleimg">
                                            <img src="/images/articles/list/img-03.jpg" alt="img description">
                                        </figure>
                                        <div class="at-article-content">
                                            <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                            <div class="at-title">
                                                <h4>Choose Any Place To Travel Anywhere</h4>
                                                <span>Jun 27, 2019</span>
                                            </div>
                                            <div class="at-description">
                                                <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav class="at-pagination">
                                <ul>
                                    <li class="at-prevpage"><a href="javascrip:void(0);"><i class="ti-angle-left"></i></a></li>
                                    <li class="at-active"><a href="javascrip:void(0);">1</a></li>
                                    <li><a href="javascrip:void(0);">2</a></li>
                                    <li><a href="javascrip:void(0);">3</a></li>
                                    <li><a href="javascrip:void(0);">4</a></li>
                                    <li><a href="javascrip:void(0);">...</a></li>
                                    <li><a href="javascrip:void(0);">50</a></li>
                                    <li class="at-nextpage"><a href="javascrip:void(0);"><i class="ti-angle-right"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Properties List End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Two Columns End -->
@endsection
