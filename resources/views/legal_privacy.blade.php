@extends('layouts.app')

@section('innerBanner')
    <!-- Inner Banner Start -->
    <div class="at-haslayout at-innerbannerholder">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-12">
                    <div class="at-innerbannercontent">
                        <div class="at-title"><h2>Some Legal Things</h2></div>
                        <ol class="at-breadcrumb">
                            <li><a href="{{ route('pages.main') }}">Main</a></li>
                            <li>Legal &amp; Privacy info</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Slider End -->
@endsection

@section('content')
    <!-- Two Columns Start -->
    <div class="at-haslayout at-main-section">
        <div class="container">
            <div class="row">
                <div id="at-twocolumns" class="at-twocolumns at-haslayout">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 float-right">
                        <aside id="at-sidebar" class="at-sidebar float-left mt-md-0">
                            <div class="at-sideholder">
                                <a href="javascript:void(0);" id="at-closesidebar" class="at-closesidebar"><i class="ti-close"></i></a>
                                <div class="at-sidescrollbar">
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Search</h2>
                                        </div>
                                        <div class="at-widgets-content at-formsearch-holder">
                                            <form class="at-formtheme at-formsearch">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <input type="text" name="Search" class="form-control" placeholder="Search Category">
                                                        <a href="javascrip:void(0);" class="at-searchgbtn"><i class="ti-search"></i></a>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="at-widgets-holder">
                                        <div class="at-widgets-title">
                                            <h2>Legal &amp; Privacy info</h2>
                                        </div>
                                        <div class="at-widgets-content">
                                            <ul class="at-legalprivacyinfo">
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color1"></i>Consectetur adipisicing elit</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color2"></i>Sed do eiusmod tempor incididunt</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color3"></i>Labore et dolore magna aliqua</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color4"></i>Ut enim ad minim veniam nostrud</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color5"></i>Exercitation ullamco laboris nisi</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color6"></i>Aliquip ex ea commodo consequat</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color1"></i>Duis aute irure dolor in reprehenderit</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color2"></i>Voluptate velit esse cillum dolore</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color3"></i>Nulla pariatur excepteur sint occaecat</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color4"></i>Cupidatat non proident</a></li>
                                                <li><a href="javascript:void(0);"><i class="fa fa-angle-right at-color5"></i>Sunt in culpa qui officia deserunt</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-8 float-left">
                        <div class="at-privacy-imgholder d-flex dc-py-15 flex-column flex-xl-row">
                            <div class="at-gridlist-option at-option-mt">
                                <a href="javascript:void(0);" id="at-btnopenclose" class="at-btnopenclose"><i class="ti-settings"></i></a>
                            </div>
                            <div class="at-privacydetails">
                                <div class="at-title">
                                    <h3>What Kind of Information We Collect?</h3>
                                </div>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis auete irure dolor in reprehenderit in voluptate velit.</p>
                                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sitame voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicoe quia consequuntur magni dolores eos qui</p>
                                </div>
                            </div>
                            <figure class="at-privacy-img"><img src="/images/img-01.jpg" alt="img description"></figure>
                        </div>
                        <div class="at-privacydetails">
                            <div class="at-title">
                                <h3>Data Usage Policy</h3>
                            </div>
                            <div class="at-description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna a aliqua. Ut enim ad minim veniam, quis nostrud at a exercitation ullamco laboris nisi ut aliquip at ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi</p>
                            </div>
                        </div>
                        <div class="at-privacydetails">
                            <div class="at-title">
                                <h3>Basic Privacy Settings &amp; Tools</h3>
                            </div>
                            <div class="at-description">
                                <p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore. Excepteur at asint occaecat cupidatat non proident, sunt in culpa qui officia mollit anim id est laborum. Sed ut perspiciatis unde omnis iste at natus error at sit voluptatem accusantium doloremque at laudantium, rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.</p>
                                <p>Weuia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eostateums qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sitam amet, consectetur, adipisci velit, sed quia non numquam eius modi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Two Columns End -->
@endsection
