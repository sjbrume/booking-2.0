@extends('layouts.app')

@section('innerBanner')
    <!-- Home Slider Start -->
    <div id="at-homesilder-holder" class="at-homeslider-holder at-haslayout">
        <div class="at-homeslidervideo">
            <video autoplay loop muted>
                <source src="/images/video.mp4" type="video/mp4">
                <source src="/images/video.mp4" type="video/ogg">
            </video>
        </div>
        <div class="at-home-banner at-home-banner-two at-haslayout">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12 push-md-0 col-lg-10 push-lg-1 col-xl-8 push-xl-2">
                        <div class="at-slider-header">
                            <div class="at-title">
                                <h1><span>Find Exotic &amp; Affordable</span>Hotel Rooms Instantly</h1>
                            </div>
                            <div class="at-description">
                                <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisiut aliquip</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-xl-11 push-xl-1">
                        <div class="at-slider-content">
                            <div class="at-title">
                                <h4>Start Your Search Here</h4>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-6 float-left">
                                    <form class="at-formtheme at-formbanner">
                                        <fieldset class="at-datetime">
                                            <div class="form-group">
                                                <div class="at-select">
                                                    <select >
                                                        <option value="" hidden>Where You Want To Stay</option>
                                                        <option value="twitter">China</option>
                                                        <option value="linkedin">France</option>
                                                        <option value="rss">Germany</option>
                                                        <option value="vimeo">Italy</option>
                                                        <option value="tumblr">Japan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="at-selectdate-holder">
                                                    <div class="at-select">
                                                        <label>Check-In:</label>
                                                        <input type="text" id="at-startdate" class="form-control" placeholder="date">
                                                    </div>
                                                    <div class="at-select">
                                                        <label>Check Out:</label>
                                                        <input type="text" id="at-enddate" class="form-control" placeholder="date">
                                                    </div>
                                                    <a href="javascript:void(0);" class="at-calendarbtn"><i class="ti-calendar"></i></a>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="at-guestsform">
                                            <legend class="at-formtitle">Guests</legend>
                                            <div class="form-group">
                                                <ul class="at-guestsinfo">
                                                    <li>
                                                        <div class="at-gueststitle">
                                                            <span>Adults</span>
                                                        </div>
                                                        <div class="at-guests-radioholder">
																<span class="at-radio">
																	<input id="at-adults1" type="radio" name="adults" value="adults" checked="">
																	<label for="at-adults1">01</label>
																</span>
                                                            <span class="at-radio">
																	<input id="at-adults2" type="radio" name="adults" value="adults2">
																	<label for="at-adults2">02</label>
																</span>
                                                            <span class="at-radio">
																	<input id="at-adults3" type="radio" name="adults" value="adults3">
																	<label for="at-adults3">03</label>
																</span>
                                                            <div class="at-dropdown">
                                                                <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                                            </div>
                                                            <div class="at-radioholder">
																	<span class="at-radio">
																		<input id="at-adults4" data-title="04" type="radio" name="adults" value="adults4">
																		<label for="at-adults4">04</label>
																	</span>
                                                                <span class="at-radio">
																		<input id="at-adults5" data-title="05" type="radio" name="adults" value="adults5">
																		<label for="at-adults5">05</label>
																	</span>
                                                                <span class="at-radio">
																		<input id="at-adults6" data-title="06" type="radio" name="adults" value="adults6">
																		<label for="at-adults6">06</label>
																	</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="at-gueststitle">
                                                            <span>Children <em>(Ages 2–12)</em></span>
                                                        </div>
                                                        <div class="at-guests-radioholder">
																<span class="at-radio">
																	<input id="at-adultsv1" type="radio" name="adultsv" value="adultsv1">
																	<label for="at-adultsv1">01</label>
																</span>
                                                            <span class="at-radio">
																	<input id="at-adultsv2" type="radio" name="adultsv" value="adultsv2">
																	<label for="at-adultsv2">02</label>
																</span>
                                                            <span class="at-radio">
																	<input id="at-adultsv3" type="radio" name="adultsv" value="adultsv3" checked="">
																	<label for="at-adultsv3">03</label>
																</span>
                                                            <div class="at-dropdown">
                                                                <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                                            </div>
                                                            <div class="at-radioholder">
																	<span class="at-radio">
																		<input id="at-adultsv4" data-title="04" type="radio" name="adultsv" value="adultsv4">
																		<label for="at-adultsv4">04</label>
																	</span>
                                                                <span class="at-radio">
																		<input id="at-adultsv5" data-title="05" type="radio" name="adultsv" value="adultsv5">
																		<label for="at-adultsv5">05</label>
																	</span>
                                                                <span class="at-radio">
																		<input id="at-adultsv6" data-title="06" type="radio" name="adultsv" value="adultsv6">
																		<label for="at-adultsv6">06</label>
																	</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="at-gueststitle">
                                                            <span>Infants <em>(Under 2)</em></span>
                                                        </div>
                                                        <div class="at-guests-radioholder">
																<span class="at-radio">
																	<input id="at-adultsv1b" type="radio" name="adultsb" value="adultsb" checked="">
																	<label for="at-adultsv1b">01</label>
																</span>
                                                            <span class="at-radio">
																	<input id="at-adultsv2b" type="radio" name="adultsb" value="adults2b">
																	<label for="at-adultsv2b">02</label>
																</span>
                                                            <span class="at-radio">
																	<input id="at-adults3b" type="radio" name="adultsb" value="adults3b">
																	<label for="at-adults3b">03</label>
																</span>
                                                            <div class="at-dropdown">
                                                                <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                                            </div>
                                                            <div class="at-radioholder">
																	<span class="at-radio">
																		<input id="at-adults4b" data-title="04" type="radio" name="adultsb" value="adults4b">
																		<label for="at-adults4b">04</label>
																	</span>
                                                                <span class="at-radio">
																		<input id="at-adults5b" data-title="05" type="radio" name="adultsb" value="adults5b">
																		<label for="at-adults5b">05</label>
																	</span>
                                                                <span class="at-radio">
																		<input id="at-adults6b" data-title="06" type="radio" name="adultsb" value="adults6b">
																		<label for="at-adults6b">06</label>
																	</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="col-12 col-md-12 col-lg-6 float-left">
                                    <form class="at-formtheme at-formbanner">
                                        <fieldset class="at-roomform">
                                            <legend class="at-formtitle">Room Type</legend>
                                            <div class="form-group">
                                                <div class="at-room-radioholder at-room-radiovtwo">
														<span class="at-radio">
															<input id="at-private" type="radio" name="private" checked>
															<label for="at-private">
																<img src="/images/radio-imgs/img-04.jpg" alt="img">
																<span><em>Private Room</em> (58,1250)</span>
															</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-shared" type="radio" name="private">
															<label for="at-shared">
																<img src="/images/radio-imgs/img-05.jpg" alt="img">
																<span><em>Shared Room</em> (31,5245)</span>
															</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-entire" type="radio" name="private">
															<label for="at-entire">
																<img src="/images/radio-imgs/img-06.jpg" alt="img">
																<span><em>Entire Place</em> (22,4523)</span>
															</label>
														</span>
                                                </div>
                                            </div>
                                            <div class="form-group at-btnarea">
                                                <a href="javascript:void(0);" class="at-btn">Search Now</a>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Slider End -->
@endsection

@section('content')
    <!-- Recommended Section Start -->
    <section class="at-haslayout at-main-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 push-md-0 col-lg-10 push-lg-1 col-xl-8 push-xl-2">
                    <div class="at-sectionhead">
                        <div class="at-sectiontitle">
                            <h2>Recommended For You</h2>
                            <span>Start Your Trip With Memory</span>
                        </div>
                        <div class="at-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisiut aliquip</p>
                        </div>
                    </div>
                </div>
                <div class="at-recommended-gallery at-haslayout">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <div class="tr-trip-imgs">
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="/images/trip-imgs/img-01.jpg" alt="img description">
                                </a>
                                <figcaption>
                                    <div class="at-trip-content">
                                        <h3>United Emirates</h3>
                                        <h4>$113 <span>(Starting From)</span></h4>
                                    </div>
                                </figcaption>
                            </figure>
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="/images/trip-imgs/img-02.jpg" alt="img description">
                                </a>
                                <figcaption>
                                    <div class="at-trip-content">
                                        <h3>China</h3>
                                        <h4>$168 <span>(Starting From)</span></h4>
                                    </div>
                                </figcaption>
                            </figure>
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="/images/trip-imgs/img-03.jpg" alt="img description">
                                </a>
                                <figcaption>
                                    <div class="at-trip-content">
                                        <h3>Sydney</h3>
                                        <h4>$140 <span>(Starting From)</span></h4>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <div class="tr-trip-imgs">
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="/images/trip-imgs/img-04.jpg" alt="img description">
                                </a>
                                <figcaption>
                                    <div class="at-trip-content">
                                        <h3>Chicago</h3>
                                        <h4>$120 <span>(Starting From)</span></h4>
                                    </div>
                                </figcaption>
                            </figure>
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="/images/trip-imgs/img-05.jpg" alt="img description">
                                </a>
                                <figcaption>
                                    <div class="at-trip-content">
                                        <h3>United Kingdom</h3>
                                        <h4>$240 <span>(Starting From)</span></h4>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <div class="tr-trip-imgs">
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="/images/trip-imgs/img-06.jpg" alt="img description">
                                </a>
                                <figcaption>
                                    <div class="at-trip-content">
                                        <h3>New York</h3>
                                        <h4>$170 <span>(Starting From)</span></h4>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 mt-last-child float-left">
                        <div class="tr-trip-imgs">
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="/images/trip-imgs/img-07.jpg" alt="img description">
                                </a>
                                <figcaption>
                                    <div class="at-trip-content">
                                        <h3>Rome</h3>
                                        <h4>$180 <span>(Starting From)</span></h4>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 at-btnarea-mt float-left">
                        <div class="at-btnarea">
                            <a href="javascript:void(0);" class="at-btn">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Recommended Section Start -->
    <!-- Top Categories Start -->
    <section class="at-haslayout at-main-section at-sectionbg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 push-md-0 col-lg-10 push-lg-1 col-xl-8 push-xl-2">
                    <div class="at-sectionhead">
                        <div class="at-sectiontitle">
                            <h2>Explore Top Categories</h2>
                            <span>Quick Way To Be There</span>
                        </div>
                        <div class="at-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisiut aliquip</p>
                        </div>
                    </div>
                </div>
                <div class="at-category-gallery at-haslayout">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <a href="javascript:void(0);">
                            <figure class="at-category-img">
                                <img src="/images/category/img-01.jpg" alt="img description">
                                <figcaption><h3>Apartments</h3><span>12,568 Listings</span></figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <a href="javascript:void(0);">
                            <figure class="at-category-img">
                                <img src="/images/category/img-02.jpg" alt="img description">
                                <figcaption><h3>Apartments</h3><span>12,568 Listings</span></figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <a href="javascript:void(0);">
                            <figure class="at-category-img">
                                <img src="/images/category/img-03.jpg" alt="img description">
                                <figcaption><h3>Apartments</h3><span>12,568 Listings</span></figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <a href="javascript:void(0);">
                            <figure class="at-category-img">
                                <img src="/images/category/img-04.jpg" alt="img description">
                                <figcaption><h3>Apartments</h3><span>12,568 Listings</span></figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <a href="javascript:void(0);">
                            <figure class="at-category-img">
                                <img src="/images/category/img-05.jpg" alt="img description">
                                <figcaption><h3>Apartments</h3><span>12,568 Listings</span></figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                        <a href="javascript:void(0);">
                            <figure class="at-category-img">
                                <img src="/images/category/img-06.jpg" alt="img description">
                                <figcaption><h3>Apartments</h3><span>12,568 Listings</span></figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-left at-btnarea">
                        <a href="javascript:void(0);" class="at-btn">View All</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Top Categories Start -->
    <!-- Featured Properties Start -->
    <section class="at-haslayout at-main-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 push-md-0 col-lg-10 push-lg-1 col-xl-8 push-xl-2">
                    <div class="at-sectionhead">
                        <div class="at-sectiontitle">
                            <h2>Featured Properties</h2>
                            <span>Treat Yourself Like a King</span>
                        </div>
                        <div class="at-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisiut aliquip</p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div id="at-featured-sliders" class="at-featured-sliders owl-carousel">
                        <div class="item">
                            <div class="at-featured-holder">
                                <div class="at-featuredslider owl-carousel">
                                    <figure class="item">
                                        <a href="javascript:void(0);"><img src="/images/featured-img/img-01.jpg" alt="img description" class="item"></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                    <figure class="item">
                                        <a href="javascript:void(0);">
                                            <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                        </a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="at-featured-content">
                                    <div class="at-featured-head">
                                        <div class="at-featured-tags"><a href="javascript:void(0);">Apartment</a> </div>
                                        <div class="at-featured-title">
                                            <h3>Mountain Retreat Room <span>$240 <em>/ night</em></span></h3>
                                            <div class="at-userimg at-verifieduser">
                                                <img src="/images/featured-img/img-04.jpg" alt="img description">
                                                <i class="fa fa-shield-alt"></i>
                                            </div>
                                        </div>
                                        <div class="at-featurerating">
                                            <span class="at-stars"><span></span></span><em>14236 review</em>
                                        </div>
                                        <ul class="at-room-featured">
                                            <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                        </ul>
                                    </div>
                                    <div class="at-featured-footer">
                                        <address>46 Morningside Road, London, UK</address>
                                        <div class="at-share-holder">
                                            <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                            <div class="at-share-option">
                                                <span>Share:</span>
                                                <ul class="at-socialicons">
                                                    <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                    <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                    <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                                <a href="javascript:void(0);">Report</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="at-featured-holder">
                                <div class="at-featuredslider owl-carousel">
                                    <figure class="item">
                                        <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8" class="at-video-icon"><i class="fab fa-youtube"></i></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                    <figure class="item">
                                        <a href="javascript:void(0);"><img src="/images/featured-img/img-01.jpg" alt="img description" class="item"></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="at-featured-content">
                                    <div class="at-featured-head">
                                        <div class="at-featured-tags"><a href="javascript:void(0);">Bed &amp; Breakfast</a> </div>
                                        <div class="at-featured-title">
                                            <h3>Balsam Fir Bungalow Suite <span>$120 <em>/ night</em></span></h3>
                                            <div class="at-userimg at-verifieduser">
                                                <img src="/images/featured-img/img-05.jpg" alt="img description">
                                                <i class="fa fa-shield-alt"></i>
                                            </div>
                                        </div>
                                        <div class="at-featurerating">
                                            <span class="at-stars"><span></span></span><em>14236 review</em>
                                        </div>
                                        <ul class="at-room-featured">
                                            <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                        </ul>
                                    </div>
                                    <div class="at-featured-footer">
                                        <address>14 Tottenham Court Road, New York</address>
                                        <div class="at-share-holder">
                                            <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                            <div class="at-share-option">
                                                <span>Share:</span>
                                                <ul class="at-socialicons">
                                                    <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                    <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                    <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                                <a href="javascript:void(0);">Report</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="at-featured-holder">
                                <div class="at-featuredslider owl-carousel">
                                    <figure class="item">
                                        <a href="javascript:void(0);"><img src="/images/featured-img/img-03.jpg" alt="img description" class="item"></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <img src="/images/featured-img/icons/360.png" alt="img description" class="at-360-img">
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                    <figure class="item">
                                        <a href="javascript:void(0);">
                                            <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                        </a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="at-featured-content">
                                    <div class="at-featured-head">
                                        <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                        <div class="at-featured-title">
                                            <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                            <div class="at-userimg at-verifieduser">
                                                <img src="/images/featured-img/img-06.jpg" alt="img description">
                                                <i class="fa fa-shield-alt"></i>
                                            </div>
                                        </div>
                                        <div class="at-featurerating">
                                            <span class="at-stars"><span></span></span><em>14236 review</em>
                                        </div>
                                        <ul class="at-room-featured">
                                            <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                        </ul>
                                    </div>
                                    <div class="at-featured-footer">
                                        <address>3 Edgar Buildings, Austrailia</address>
                                        <div class="at-share-holder">
                                            <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                            <div class="at-share-option">
                                                <span>Share:</span>
                                                <ul class="at-socialicons">
                                                    <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                    <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                    <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                                <a href="javascript:void(0);">Report</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="at-featured-holder">
                                <div class="at-featuredslider owl-carousel">
                                    <figure class="item">
                                        <a href="javascript:void(0);"><img src="/images/featured-img/img-01.jpg" alt="img description" class="item"></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                    <figure class="item">
                                        <a href="javascript:void(0);">
                                            <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                        </a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="at-featured-content">
                                    <div class="at-featured-head">
                                        <div class="at-featured-tags"><a href="javascript:void(0);">Apartment</a> </div>
                                        <div class="at-featured-title">
                                            <h3>Mountain Retreat Room <span>$240 <em>/ night</em></span></h3>
                                            <div class="at-userimg at-verifieduser">
                                                <img src="/images/featured-img/img-04.jpg" alt="img description">
                                                <i class="fa fa-shield-alt"></i>
                                            </div>
                                        </div>
                                        <div class="at-featurerating">
                                            <span class="at-stars"><span></span></span><em>14236 review</em>
                                        </div>
                                        <ul class="at-room-featured">
                                            <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                        </ul>
                                    </div>
                                    <div class="at-featured-footer">
                                        <address>46 Morningside Road, London, UK</address>
                                        <div class="at-share-holder">
                                            <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                            <div class="at-share-option">
                                                <span>Share:</span>
                                                <ul class="at-socialicons">
                                                    <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                    <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                    <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                                <a href="javascript:void(0);">Report</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="at-featured-holder">
                                <div class="at-featuredslider owl-carousel">
                                    <figure class="item">
                                        <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                        <a data-rel="prettyPhoto[video]" href="https://youtu.be/XxxIEGzhIG8" class="at-video-icon"><i class="fab fa-youtube"></i></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                    <figure class="item">
                                        <a href="javascript:void(0);"><img src="/images/featured-img/img-01.jpg" alt="img description" class="item"></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="at-featured-content">
                                    <div class="at-featured-head">
                                        <div class="at-featured-tags"><a href="javascript:void(0);">Bed &amp; Breakfast</a> </div>
                                        <div class="at-featured-title">
                                            <h3>Balsam Fir Bungalow Suite <span>$120 <em>/ night</em></span></h3>
                                            <div class="at-userimg at-verifieduser">
                                                <img src="/images/featured-img/img-05.jpg" alt="img description">
                                                <i class="fa fa-shield-alt"></i>
                                            </div>
                                        </div>
                                        <div class="at-featurerating">
                                            <span class="at-stars"><span></span></span><em>14236 review</em>
                                        </div>
                                        <ul class="at-room-featured">
                                            <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                        </ul>
                                    </div>
                                    <div class="at-featured-footer">
                                        <address>14 Tottenham Court Road, New York</address>
                                        <div class="at-share-holder">
                                            <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                            <div class="at-share-option">
                                                <span>Share:</span>
                                                <ul class="at-socialicons">
                                                    <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                    <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                    <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                                <a href="javascript:void(0);">Report</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="at-featured-holder">
                                <div class="at-featuredslider owl-carousel">
                                    <figure class="item">
                                        <a href="javascript:void(0);"><img src="/images/featured-img/img-03.jpg" alt="img description" class="item"></a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <img src="/images/featured-img/icons/360.png" alt="img description" class="at-360-img">
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>04 Photos</span>
                                                <a href="javascript:void(0);" class="at-like at-liked">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                    <figure class="item">
                                        <a href="javascript:void(0);">
                                            <img src="/images/featured-img/img-02.jpg" alt="img description" class="item">
                                        </a>
                                        <figcaption>
                                            <div class="at-slider-details">
                                                <a href="javascript:void(0);" class="at-tag">Featured</a>
                                                <a href="javascript:void(0);" class="at-tag at-rated">Top Rated</a>
                                                <span class="at-photolayer"><i class="fas fa-layer-group"></i>19 Photos</span>
                                                <a href="javascript:void(0);" class="at-like">Saved<i class="far fa-heart"></i></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="at-featured-content">
                                    <div class="at-featured-head">
                                        <div class="at-featured-tags"><a href="javascript:void(0);">Condo</a> </div>
                                        <div class="at-featured-title">
                                            <h3>Portland-Plush KING Room <span>$80 <em>/ night</em></span></h3>
                                            <div class="at-userimg at-verifieduser">
                                                <img src="/images/featured-img/img-06.jpg" alt="img description">
                                                <i class="fa fa-shield-alt"></i>
                                            </div>
                                        </div>
                                        <div class="at-featurerating">
                                            <span class="at-stars"><span></span></span><em>14236 review</em>
                                        </div>
                                        <ul class="at-room-featured">
                                            <li><span><i><img src="/images/featured-img/icons/img-01.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-02.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-03.jpg" alt="img description"></i> 04 Guests</span></li>
                                            <li><span><i><img src="/images/featured-img/icons/img-04.jpg" alt="img description"></i> 02 Bedrooms</span></li>
                                        </ul>
                                    </div>
                                    <div class="at-featured-footer">
                                        <address>3 Edgar Buildings, Austrailia</address>
                                        <div class="at-share-holder">
                                            <a href="javascript:void(0);"><i class="ti-more-alt"></i></a>
                                            <div class="at-share-option">
                                                <span>Share:</span>
                                                <ul class="at-socialicons">
                                                    <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                                    <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                                    <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                                <a href="javascript:void(0);">Report</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Featured Properties Start -->
    <!-- Testimonials Start -->
    <section class="at-haslayout at-main-section at-sectionbg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 push-md-0 col-lg-10 push-lg-1 col-xl-8 push-xl-2">
                    <div class="at-sectionhead">
                        <div class="at-sectiontitle">
                            <h2>Words From Clients</h2>
                            <span>Client’s Great Feedback</span>
                        </div>
                        <div class="at-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisiut aliquip</p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div id="at-testimonials-slider" class="at-testimonials-slider owl-carousel">
                        <div class="at-testimonials-content item">
                            <div class="at-description">
                                <blockquote>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="28" viewBox="0 0 78 28">
                                        <image id="img1" data-name="“”" width="78" height="28" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE4AAAAcCAMAAAD4MnnTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAM1BMVEUAAAD4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCsAAABoLGZQAAAAD3RSTlMAZu7dVRF3mUTMqogiuzPnYEgLAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAa1JREFUSMeNVtGWhSAILE3TtPj/v93aBAfonF0euc4wDNC5y/LGGihui4otUlgXEylHItoL5nK9Uwc+bfSE4lt/U02zlUBvHJLqcaTOJPxvIgDwGo+6YiMJ5utBUjVpKGDlFbaV4qSj0dwJqaZfgbqDPtRlgFK0eu9I07g7sjHOelcV9lJYIegeKnpPNYjgsKrX19CdnfRdxaToFJR2TxdBHLjE4swmanWnpwtTyTRu2T5btd6d3juab6AvHmsxdNnTFUPHO3f4pqJh03s3tOtupSKcHFdolm650L1XQMcSUZqvudieQnZfgAvsG9US6DtRLN8w2FsvS7hmViizK7lKBaW16wN7FDo+qQa/rFLBD0u7azZ5LiV+fg6poAdd/LCyYePVgFVIbI09nObpqqHjX+Fi4BL+czgYfDGwlHimH4ezqxR9bzJ4wBS7GeOg07ek1bFymJDsVXfYhlfx4Z1A54JnhU0KW3AV/GTFJRgrmz/0orxT2226mksSuq/AWz2HIa9gGOoj1Q0U1EkTcsNx1mSPgvnkNWOctIJfn+0Zb1Xrfz3AkN2F/fHn4wcR1UH+/Sb0lwAAAABJRU5ErkJggg=="/>
                                    </svg>
                                    <q>Consectetur adipisicing elitenu sed  eiusmod tempor do incididunt ut labore etna dolore magna aliqua enim minim veniamat quis nostrud exercitation ullamco laboris.</q>
                                </blockquote>
                            </div>
                            <figure>
                                <img src="/images/testimonials/img-01.jpg" alt="img description">
                            </figure>
                            <div class="at-testimonials-title">
                                <h3><a href="javascript:void(0);">Marivel Rosenberry</a></h3>
                                <span>United Kingdom</span>
                            </div>
                        </div>
                        <div class="at-testimonials-content item">
                            <div class="at-description">
                                <blockquote>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="28" viewBox="0 0 78 28">
                                        <image id="img2" data-name="“”" width="78" height="28" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE4AAAAcCAMAAAD4MnnTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAM1BMVEUAAAD4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCsAAABoLGZQAAAAD3RSTlMAZu7dVRF3mUTMqogiuzPnYEgLAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAa1JREFUSMeNVtGWhSAILE3TtPj/v93aBAfonF0euc4wDNC5y/LGGihui4otUlgXEylHItoL5nK9Uwc+bfSE4lt/U02zlUBvHJLqcaTOJPxvIgDwGo+6YiMJ5utBUjVpKGDlFbaV4qSj0dwJqaZfgbqDPtRlgFK0eu9I07g7sjHOelcV9lJYIegeKnpPNYjgsKrX19CdnfRdxaToFJR2TxdBHLjE4swmanWnpwtTyTRu2T5btd6d3juab6AvHmsxdNnTFUPHO3f4pqJh03s3tOtupSKcHFdolm650L1XQMcSUZqvudieQnZfgAvsG9US6DtRLN8w2FsvS7hmViizK7lKBaW16wN7FDo+qQa/rFLBD0u7azZ5LiV+fg6poAdd/LCyYePVgFVIbI09nObpqqHjX+Fi4BL+czgYfDGwlHimH4ezqxR9bzJ4wBS7GeOg07ek1bFymJDsVXfYhlfx4Z1A54JnhU0KW3AV/GTFJRgrmz/0orxT2226mksSuq/AWz2HIa9gGOoj1Q0U1EkTcsNx1mSPgvnkNWOctIJfn+0Zb1Xrfz3AkN2F/fHn4wcR1UH+/Sb0lwAAAABJRU5ErkJggg=="/>
                                    </svg>
                                    <q>Consectetur adipisicing elitenu sed  eiusmod tempor do incididunt ut labore etna dolore magna aliqua enim minim veniamat quis nostrud exercitation ullamco laboris.</q>
                                </blockquote>
                            </div>
                            <figure>
                                <img src="/images/testimonials/img-02.jpg" alt="img description">
                            </figure>
                            <div class="at-testimonials-title">
                                <h3><a href="javascript:void(0);">Vince Hammel</a></h3>
                                <span>United Emirates</span>
                            </div>
                        </div>
                        <div class="at-testimonials-content item">
                            <div class="at-description">
                                <blockquote>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="28" viewBox="0 0 78 28">
                                        <image id="img3" data-name="“”" width="78" height="28" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE4AAAAcCAMAAAD4MnnTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAM1BMVEUAAAD4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCsAAABoLGZQAAAAD3RSTlMAZu7dVRF3mUTMqogiuzPnYEgLAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAa1JREFUSMeNVtGWhSAILE3TtPj/v93aBAfonF0euc4wDNC5y/LGGihui4otUlgXEylHItoL5nK9Uwc+bfSE4lt/U02zlUBvHJLqcaTOJPxvIgDwGo+6YiMJ5utBUjVpKGDlFbaV4qSj0dwJqaZfgbqDPtRlgFK0eu9I07g7sjHOelcV9lJYIegeKnpPNYjgsKrX19CdnfRdxaToFJR2TxdBHLjE4swmanWnpwtTyTRu2T5btd6d3juab6AvHmsxdNnTFUPHO3f4pqJh03s3tOtupSKcHFdolm650L1XQMcSUZqvudieQnZfgAvsG9US6DtRLN8w2FsvS7hmViizK7lKBaW16wN7FDo+qQa/rFLBD0u7azZ5LiV+fg6poAdd/LCyYePVgFVIbI09nObpqqHjX+Fi4BL+czgYfDGwlHimH4ezqxR9bzJ4wBS7GeOg07ek1bFymJDsVXfYhlfx4Z1A54JnhU0KW3AV/GTFJRgrmz/0orxT2226mksSuq/AWz2HIa9gGOoj1Q0U1EkTcsNx1mSPgvnkNWOctIJfn+0Zb1Xrfz3AkN2F/fHn4wcR1UH+/Sb0lwAAAABJRU5ErkJggg=="/>
                                    </svg>
                                    <q>Consectetur adipisicing elitenu sed  eiusmod tempor do incididunt ut labore etna dolore magna aliqua enim minim veniamat quis nostrud exercitation ullamco laboris.</q>
                                </blockquote>
                            </div>
                            <figure>
                                <img src="/images/testimonials/img-03.jpg" alt="img description">
                            </figure>
                            <div class="at-testimonials-title">
                                <h3><a href="javascript:void(0);">Margarito Beverage</a></h3>
                                <span>Austrailia</span>
                            </div>
                        </div>
                        <div class="at-testimonials-content item">
                            <div class="at-description">
                                <blockquote>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="28" viewBox="0 0 78 28">
                                        <image id="img4" data-name="“”" width="78" height="28" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE4AAAAcCAMAAAD4MnnTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAM1BMVEUAAAD4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCsAAABoLGZQAAAAD3RSTlMAZu7dVRF3mUTMqogiuzPnYEgLAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAa1JREFUSMeNVtGWhSAILE3TtPj/v93aBAfonF0euc4wDNC5y/LGGihui4otUlgXEylHItoL5nK9Uwc+bfSE4lt/U02zlUBvHJLqcaTOJPxvIgDwGo+6YiMJ5utBUjVpKGDlFbaV4qSj0dwJqaZfgbqDPtRlgFK0eu9I07g7sjHOelcV9lJYIegeKnpPNYjgsKrX19CdnfRdxaToFJR2TxdBHLjE4swmanWnpwtTyTRu2T5btd6d3juab6AvHmsxdNnTFUPHO3f4pqJh03s3tOtupSKcHFdolm650L1XQMcSUZqvudieQnZfgAvsG9US6DtRLN8w2FsvS7hmViizK7lKBaW16wN7FDo+qQa/rFLBD0u7azZ5LiV+fg6poAdd/LCyYePVgFVIbI09nObpqqHjX+Fi4BL+czgYfDGwlHimH4ezqxR9bzJ4wBS7GeOg07ek1bFymJDsVXfYhlfx4Z1A54JnhU0KW3AV/GTFJRgrmz/0orxT2226mksSuq/AWz2HIa9gGOoj1Q0U1EkTcsNx1mSPgvnkNWOctIJfn+0Zb1Xrfz3AkN2F/fHn4wcR1UH+/Sb0lwAAAABJRU5ErkJggg=="/>
                                    </svg>
                                    <q>Consectetur adipisicing elitenu sed  eiusmod tempor do incididunt ut labore etna dolore magna aliqua enim minim veniamat quis nostrud exercitation ullamco laboris.</q>
                                </blockquote>
                            </div>
                            <figure>
                                <img src="/images/testimonials/img-01.jpg" alt="img description">
                            </figure>
                            <div class="at-testimonials-title">
                                <h3><a href="javascript:void(0);">Marivel Rosenberry</a></h3>
                                <span>United Kingdom</span>
                            </div>
                        </div>
                        <div class="at-testimonials-content item">
                            <div class="at-description">
                                <blockquote>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="78" height="28" viewBox="0 0 78 28">
                                        <image id="img5" data-name="“”" width="78" height="28" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE4AAAAcCAMAAAD4MnnTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAM1BMVEUAAAD4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCv4tCsAAABoLGZQAAAAD3RSTlMAZu7dVRF3mUTMqogiuzPnYEgLAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAa1JREFUSMeNVtGWhSAILE3TtPj/v93aBAfonF0euc4wDNC5y/LGGihui4otUlgXEylHItoL5nK9Uwc+bfSE4lt/U02zlUBvHJLqcaTOJPxvIgDwGo+6YiMJ5utBUjVpKGDlFbaV4qSj0dwJqaZfgbqDPtRlgFK0eu9I07g7sjHOelcV9lJYIegeKnpPNYjgsKrX19CdnfRdxaToFJR2TxdBHLjE4swmanWnpwtTyTRu2T5btd6d3juab6AvHmsxdNnTFUPHO3f4pqJh03s3tOtupSKcHFdolm650L1XQMcSUZqvudieQnZfgAvsG9US6DtRLN8w2FsvS7hmViizK7lKBaW16wN7FDo+qQa/rFLBD0u7azZ5LiV+fg6poAdd/LCyYePVgFVIbI09nObpqqHjX+Fi4BL+czgYfDGwlHimH4ezqxR9bzJ4wBS7GeOg07ek1bFymJDsVXfYhlfx4Z1A54JnhU0KW3AV/GTFJRgrmz/0orxT2226mksSuq/AWz2HIa9gGOoj1Q0U1EkTcsNx1mSPgvnkNWOctIJfn+0Zb1Xrfz3AkN2F/fHn4wcR1UH+/Sb0lwAAAABJRU5ErkJggg=="/>
                                    </svg>
                                    <q>Consectetur adipisicing elitenu sed  eiusmod tempor do incididunt ut labore etna dolore magna aliqua enim minim veniamat quis nostrud exercitation ullamco laboris.</q>
                                </blockquote>
                            </div>
                            <figure>
                                <img src="/images/testimonials/img-02.jpg" alt="img description">
                            </figure>
                            <div class="at-testimonials-title">
                                <h3><a href="javascript:void(0);">Vince Hammel</a></h3>
                                <span>United Emirates</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonials End -->
    <!-- Articles Start -->
    <section class="at-haslayout at-main-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 push-md-0 col-lg-10 push-lg-1 col-xl-8 push-xl-2">
                    <div class="at-sectionhead">
                        <div class="at-sectiontitle">
                            <h2>Latest Articles &amp; Tips</h2>
                            <span>Stay Updated With Blogs</span>
                        </div>
                        <div class="at-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisiut aliquip</p>
                        </div>
                    </div>
                </div>
                <div class="at-articles">
                    <div class="col-12 col-md-6 col-lg-4 float-left">
                        <div class="at-article">
                            <figure class="at-articleimg">
                                <img src="/images/articles/img-01.jpg" alt="img description">
                                <figcaption><a href="javascript:void(0);" class="at-tag">Featured</a></figcaption>
                            </figure>
                            <div class="at-article-content">
                                <div class="at-featured-tags"><a href="javascript:void(0);">Admin</a> </div>
                                <div class="at-title">
                                    <h4>10 Things All Rooms Should Have</h4>
                                    <span>Jun 27, 2019</span>
                                </div>
                                <div class="at-description">
                                    <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 float-left">
                        <div class="at-article">
                            <figure class="at-articleimg">
                                <img src="/images/articles/img-02.jpg" alt="img description">
                            </figure>
                            <div class="at-article-content">
                                <div class="at-featured-tags"><a href="javascript:void(0);">Margarito Beverage</a> </div>
                                <div class="at-title">
                                    <h4>Best Days To Book Hotel For Tour</h4>
                                    <span>Jun 27, 2019</span>
                                </div>
                                <div class="at-description">
                                    <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 float-left">
                        <div class="at-article">
                            <figure class="at-articleimg">
                                <img src="/images/articles/img-03.jpg" alt="img description">
                            </figure>
                            <div class="at-article-content">
                                <div class="at-featured-tags"><a href="javascript:void(0);">Marivel Rosenberry</a> </div>
                                <div class="at-title">
                                    <h4>Choose Any Place To Travel Anywhere</h4>
                                    <span>Jun 27, 2019</span>
                                </div>
                                <div class="at-description">
                                    <p>Consectetur adipisicing elitm sed at esmod tempor incididunt a labore alor...<a href="javascript:void(0)">[more]</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Articles End -->
@endsection
