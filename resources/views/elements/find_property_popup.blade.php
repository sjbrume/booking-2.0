<!-- Find Property Popup Start-->
<div class="modal fade at-findpropertypopup" tabindex="-1" role="dialog" id="findproperty" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="at-modalcontent modal-content">
            <div class="at-slider-content">
                <div class="at-title">
                    <h4>Start Your Search Here</h4>
                    <a href="javascript:void(0);" class="at-closebtn close"><i class="lnr lnr-cross" data-dismiss="modal"></i></a>
                </div>
                <div class="at-findpropertypopup-content at-haslayout">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 float-left">
                            <form class="at-formtheme at-formbanner">
                                <fieldset class="at-datetime">
                                    <div class="form-group">
                                        <div class="at-select">
                                            <select >
                                                <option value="" hidden>Where You Want To Stay</option>
                                                <option value="twitter">China</option>
                                                <option value="linkedin">France</option>
                                                <option value="rss">Germany</option>
                                                <option value="vimeo">Italy</option>
                                                <option value="tumblr">Japan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="at-selectdate-holder">
                                            <div class="at-select">
                                                <label>Check-In:</label>
                                                <input type="text" id="at-startdate" class="form-control" placeholder="date">
                                            </div>
                                            <div class="at-select">
                                                <label>Check-Out:</label>
                                                <input type="text" id="at-enddate" class="form-control" placeholder="date">
                                            </div>
                                            <a href="javascript:void(0);" class="at-calendarbtn"><i class="ti-calendar"></i></a>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="at-guestsform">
                                    <legend class="at-formtitle">Guests</legend>
                                    <div class="form-group">
                                        <ul class="at-guestsinfo">
                                            <li>
                                                <div class="at-gueststitle">
                                                    <span>Adults</span>
                                                </div>
                                                <div class="at-guests-radioholder">
														<span class="at-radio">
															<input id="at-adults1p" type="radio" name="adults" value="adults" checked="">
															<label for="at-adults1p">01</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-adults2p" type="radio" name="adults" value="adults2">
															<label for="at-adults2p">02</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-adults3p" type="radio" name="adults" value="adults3">
															<label for="at-adults3p">03</label>
														</span>
                                                    <div class="at-dropdown">
                                                        <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                                    </div>
                                                    <div class="at-radioholder">
															<span class="at-radio">
																<input id="at-adults4p" data-title="04" type="radio" name="adults" value="adults4">
																<label for="at-adults4p">04</label>
															</span>
                                                        <span class="at-radio">
																<input id="at-adults5p" data-title="05" type="radio" name="adults" value="adults5">
																<label for="at-adults5p">05</label>
															</span>
                                                        <span class="at-radio">
																<input id="at-adults6p" data-title="06" type="radio" name="adults" value="adults6">
																<label for="at-adults6p">06</label>
															</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="at-gueststitle">
                                                    <span>Children <em>(Ages 2–12)</em></span>
                                                </div>
                                                <div class="at-guests-radioholder">
														<span class="at-radio">
															<input id="at-adultsv1p" type="radio" name="adultsv" value="adultsv1">
															<label for="at-adultsv1p">01</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-adultsv2p" type="radio" name="adultsv" value="adultsv2">
															<label for="at-adultsv2p">02</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-adultsv3p" type="radio" name="adultsv" value="adultsv3" checked="">
															<label for="at-adultsv3p">03</label>
														</span>
                                                    <div class="at-dropdown">
                                                        <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                                    </div>
                                                    <div class="at-radioholder">
															<span class="at-radio">
																<input id="at-adultsv4p" data-title="04" type="radio" name="adultsv" value="adultsv4">
																<label for="at-adultsv4p">04</label>
															</span>
                                                        <span class="at-radio">
																<input id="at-adultsv5p" data-title="05" type="radio" name="adultsv" value="adultsv5">
																<label for="at-adultsv5p">05</label>
															</span>
                                                        <span class="at-radio">
																<input id="at-adultsv6p" data-title="06" type="radio" name="adultsv" value="adultsv6">
																<label for="at-adultsv6p">06</label>
															</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="at-gueststitle">
                                                    <span>Infants <em>(Under 2)</em></span>
                                                </div>
                                                <div class="at-guests-radioholder">
														<span class="at-radio">
															<input id="at-adultsv11" type="radio" name="adultsb" value="adultsb" checked="">
															<label for="at-adultsv11">01</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-adultsv2bp" type="radio" name="adultsb" value="adults2b">
															<label for="at-adultsv2bp">02</label>
														</span>
                                                    <span class="at-radio">
															<input id="at-adults3bp" type="radio" name="adultsb" value="adults3b">
															<label for="at-adults3bp">03</label>
														</span>
                                                    <div class="at-dropdown">
                                                        <span><em class="selected-search-type">Other </em><i class="ti-angle-down"></i></span>
                                                    </div>
                                                    <div class="at-radioholder">
															<span class="at-radio">
																<input id="at-adults4bp" data-title="04" type="radio" name="adultsb" value="adults4b">
																<label for="at-adults4bp">04</label>
															</span>
                                                        <span class="at-radio">
																<input id="at-adults5bp" data-title="05" type="radio" name="adultsb" value="adults5b">
																<label for="at-adults5bp">05</label>
															</span>
                                                        <span class="at-radio">
																<input id="at-adults6bp" data-title="06" type="radio" name="adultsb" value="adults6b">
																<label for="at-adults6bp">06</label>
															</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 float-left">
                            <form class="at-formtheme at-formbanner">
                                <fieldset class="at-roomform">
                                    <legend class="at-formtitle">Room Type</legend>
                                    <div class="form-group">
                                        <div class="at-room-radioholder at-room-radiovtwo">
												<span class="at-radio">
													<input id="at-privatep" type="radio" name="privatep" checked>
													<label for="at-privatep">
														<img src="/images/radio-imgs/img-04.jpg" alt="img">
														<span><em>Private Room</em> (58,1250)</span>
													</label>
												</span>
                                            <span class="at-radio">
													<input id="at-sharedp" type="radio" name="privatep">
													<label for="at-sharedp">
														<img src="/images/radio-imgs/img-05.jpg" alt="img">
														<span><em>Shared Room</em> (31,5245)</span>
													</label>
												</span>
                                            <span class="at-radio">
													<input id="at-entirep" type="radio" name="privatep">
													<label for="at-entirep">
														<img src="/images/radio-imgs/img-06.jpg" alt="img">
														<span><em>Entire Place</em> (22,4523)</span>
													</label>
												</span>
                                        </div>
                                    </div>
                                    <div class="form-group at-btnarea">
                                        <a href="javascript:void(0);" class="at-btn">Search Now</a>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Find Property Popup End-->
