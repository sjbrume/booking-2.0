<!-- Register Popup Start-->
<div class="modal fade at-loginpopup" tabindex="-1" role="dialog" id="registerpopup" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="at-modalcontent modal-content">
            <div class="at-popuptitle">
                <h4>Register</h4>
                <a href="javascript:void(0);" class="at-closebtn close"><i class="lnr lnr-cross" data-dismiss="modal"></i></a>
            </div>
            <div class="modal-body">
                <form class="at-formtheme at-formlogin">
                    <fieldset>
                        <div class="form-group">
                            <input type="text" name="email" value="" class="form-control" placeholder="Your Email*" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" value="" class="form-control" placeholder="Password*">
                        </div>
                        <div class="form-group">
                            <input type="password" value="" class="form-control" placeholder="Retype Password*">
                        </div>
                        <div class="form-group at-btnarea">
                            <button type="submit" class="at-btn">Register</button>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <div class="at-popup-footerterms">
                    <span>By signing in  you agree to these <a href="javascript:void(0);"> Terms &amp; Conditions</a> &amp; consent to<a href="javascript:void(0);"> Cookie Policy &amp; Privacy Policy.</a></span>
                </div>
                <div class="at-loginfooterinfo">
                    <a href="javascript:void(0);"><em>Have Account?</em> Login Now</a>
                    <a href="javascript:;" class="at-forgot-password">Forgot Your Password?</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Register Popup End-->
