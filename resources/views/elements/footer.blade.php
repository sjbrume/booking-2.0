<!-- Footer Start -->
<footer id="at-footer" class="at-footer at-haslayout">
    <div class="at-fthreecolumn at-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-2">
                    <div class="at-fwidget">
                        <div class="at-fwidget-titile">
                            <h3>About</h3>
                        </div>
                        <div class="at-fwidget-content">
                            <ul>
                                <li><a href="javascript:void(0);">Our Vision</a></li>
                                <li><a href="javascript:void(0);">Our Mission</a></li>
                                <li><a href="javascript:void(0);">Term &amp; Conditions</a></li>
                                <li><a href="javascript:void(0);">Privacy Policy</a></li>
                                <li><a href="javascript:void(0);">Posting Policy</a></li>
                                <li><a href="javascript:void(0);">Team Behind Curtain</a></li>
                                <li><a href="javascript:void(0);">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-2">
                    <div class="at-fwidget">
                        <div class="at-fwidget-titile">
                            <h3>Category To Start</h3>
                        </div>
                        <div class="at-fwidget-content">
                            <ul>
                                <li><a href="javascript:void(0);">Apartment</a></li>
                                <li><a href="javascript:void(0);">Bed &amp; Breakfast</a></li>
                                <li><a href="javascript:void(0);">Condo</a></li>
                                <li><a href="javascript:void(0);">House</a></li>
                                <li><a href="javascript:void(0);">Loft</a></li>
                                <li><a href="javascript:void(0);">Studio</a></li>
                                <li class="at-showall-link"><a href="javascript:void(0);">Show All</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-6 col-xl-4">
                    <div class="at-fwidget at-locations-info">
                        <div class="at-fwidget-titile">
                            <h3>Explore Top Locations</h3>
                        </div>
                        <div class="at-fwidget-content">
                            <ul>
                                <li><a href="javascript:void(0);">United Emirates</a></li>
                                <li><a href="javascript:void(0);">Paris</a></li>
                                <li><a href="javascript:void(0);">New York City</a></li>
                                <li><a href="javascript:void(0);">Yellowstone</a></li>
                                <li><a href="javascript:void(0);">Argentine Patagonia</a></li>
                                <li><a href="javascript:void(0);">Rome</a></li>
                                <li><a href="javascript:void(0);">Sydney</a></li>
                                <li><a href="javascript:void(0);">Tahiti</a></li>
                                <li><a href="javascript:void(0);">Hong Kong</a></li>
                                <li><a href="javascript:void(0);">London</a></li>
                                <li><a href="javascript:void(0);">Rio de Janeiro</a></li>
                                <li><a href="javascript:void(0);">New Zealand</a></li>
                                <li><a href="javascript:void(0);">British Virgin Islands</a></li>
                                <li><a href="javascript:void(0);">Grand Canyon</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-6 col-xl-4">
                    <div class="at-ffollow-holder">
                        <div class="at-fwidget-titile">
                            <h3>Follow Us</h3>
                        </div>
                        <ul class="at-socialicons at-socialicons-white">
                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                            <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                            <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                        <div class="at-fnewsletter">
                            <div class="at-fwidget-titile">
                                <h3>Signup For Newsletter</h3>
                            </div>
                            <div class="at-description">
                                <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna</p>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Emaill Address" required="">
                                <button type="submit" class="at-submitbtn"><i class="ti-email"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="at-footerbottom at-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="at-copyrights-holder">
                        <div class="at-flogoarea">
                            <strong class="at-flogo"><a href="{{ route('pages.main') }}"><img src="/images/flogo.png" alt="footer logo"></a></strong>
                            <p class="at-copyrights">Copyrights © 2019 by <a href="javascript:void(0);">Tenanto.</a> All Rights Reserved.</p>
                        </div>
                        <div class="at-rightarea">
                            <figure class="at-payment-icons">
                                <img src="/images/payment-icon/img-01.jpg" alt="img description">
                                <img src="/images/payment-icon/img-02.jpg" alt="img description">
                                <img src="/images/payment-icon/img-03.jpg" alt="img description">
                                <img src="/images/payment-icon/img-04.jpg" alt="img description">
                                <img src="/images/payment-icon/img-05.jpg" alt="img description">
                                <img src="/images/payment-icon/img-06.jpg" alt="img description">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->
