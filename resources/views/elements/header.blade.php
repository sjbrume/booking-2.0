<!-- Header Start -->
<header id="at-header" class="at-header at-haslayout">
    <div class="at-topbarholder">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="at-topbar">
                        <div class="at-topcominfo">
                            <a href="tel:(+1)2345678900" class="at-callnum"><em>Call Us:</em> (+1) 2345 67 89 00</a>
                            <ul class="at-socialicons">
                                <li class="at-facebook"><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="at-twitter"><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                <li class="at-youtube"><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
                                <li class="at-instagram"><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <div class="at-loginarea float-right">
                            <a href="javascript:void(0);" class="at-loginoption" data-toggle="modal" data-target="#loginpopup">Login</a>
                            <a href="javascript:void(0);" class="at-registeroption" data-toggle="modal" data-target="#registerpopup">Register</a>
                            <div class="at-detailsbtn-topbar">
                                <a href="javascript:void(0);" class="at-btn at-btnactive">Became a Host</a>
                                <em>OR</em>
                                <a href="javascript:void(0);" class="at-btn at-btnactive at-btntwo" data-toggle="modal" data-target="#findproperty">Find Property</a>
                            </div>
                            <div class="at-userlogin">
                                <figure><img src="/images/user-img.png" alt="user img"></figure>
                                <nav class="at-usernav">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-dashboard"></i><span>Insights</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-user"></i><span>Profile Settings</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-align-justify"></i><span> My Properties</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-settings"></i><span>Add Property</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-shopping-cart"></i><span>Offers &amp; Mesages</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-user"></i><span>Payments</span>
                                            </a>
                                        </li>
                                        <li class="menu-item-has-children page_item_has_children">
                                            <a href="javascript:void(0);">
                                                <i class="ti-email"></i><span>My Favorites</span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="javascript:void(0);">Favorites Type 1</a></li>
                                                <li><a href="javascript:void(0);">Favorites Type 2</a></li>
                                                <li><a href="javascript:void(0);">Favorites Type 3</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-bookmark"></i><span>Privacy Settings</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-shift-right"></i><span>Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="at-navigationarea">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <strong class="at-logo"><a href="{{ route('pages.main') }}"><img src="/images/logo.png" alt="company logo here"></a></strong>
                    <div class="at-rightarea">
                        <nav id="at-nav" class="at-nav navbar-expand-lg">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="lnr lnr-menu"></i>
                            </button>
                            <div class="collapse navbar-collapse at-navigation" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="menu-item-has-children page_item_has_children">
                                        <a href="javascript:void(0);">Main</a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="{{ route('pages.main') }}">Home V1</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('pages.home', ['v2'], false) }}">Home V2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children page_item_has_children">
                                        <a href="javascript:void(0);">Properties</a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="{{ route('pages.propertyList') }}">property list</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('pages.propertyGrid') }}">property grid</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('pages.propertyGridMap') }}">propertygrid with map</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('pages.propertySingle') }}">property single</a>
                                            </li>
                                            <li class="menu-item-has-children page_item_has_children">
                                                <a href="javascript:void(0);">Categories</a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="javascript:void(0);">Show All</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Apartment</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Bed &amp; Breakfast</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Condo</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">House</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">loft</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Studio</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('pages.howItWorks') }}">How it works?</a>
                                    </li>
                                    <li class="nav-item at-navactive">
                                        <a href="{{ route('pages.about') }}">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('pages.contactUs') }}">Contact</a>
                                    </li>
                                    <li class="menu-item-has-children page_item_has_children">
                                        <a href="javascript:void(0);" class="at-menu-icon">
													<span>
														<i></i>
														<i></i>
														<i></i>
													</span>
                                            <span class="at-navmorecontent">pages</span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="{{ route('pages.legalPrivacy') }}">legal privacy</a>
                                            </li>
                                            <li class="menu-item-has-children page_item_has_children">
                                                <a href="javascript:void(0);">blog</a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="{{ route('pages.blogList') }}">blog list</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('pages.blogGrid') }}">blog grid</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('pages.blogSingle') }}">blog single</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="{{ route('pages.comingSoon') }}">coming soon</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header End -->
