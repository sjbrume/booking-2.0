<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang="zxx"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/fontawesome/fontawesome-all.css">
    <link rel="stylesheet" href="/css/linearicons.css">
    <link rel="stylesheet" href="/css/themify-icons.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/fullcalendar.min.css">
    <link rel="stylesheet" href="/css/prettyPhoto.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/tipso.css">
    <link rel="stylesheet" href="/css/lightpick.css">
    <link rel="stylesheet" href="/css/main-min.css">
    <link rel="stylesheet" href="/css/transitions.css">
    <link rel="stylesheet" href="/css/responsive-min.css">
    @yield('styles')
    <script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
    @include('elements.preloader')
    <!-- Wrapper Start -->
    <div id="at-wrapper" class="at-wrapper at-haslayout">
        @include('elements.header')
        @yield('innerBanner')
        <!-- Main Start -->
        <main id="at-main" class="at-main at-haslayout">
            @yield('content')
        </main>
        <!-- Main End -->
        @include('elements.footer')
    </div>
    <!-- Wrapper End -->
    @include('elements.login_popup')
    @include('elements.register_popup')
    @include('elements.find_property_popup')

    <script src="/js/vendor/jquery-library.js"></script>
    <script src="/js/vendor/bootstrap.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/prettyPhoto.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/fullcalendar.min.js"></script>
    <script src="/js/prettyPhoto.js"></script>
    <script src="/js/readmore.js"></script>
    <script src="/js/countTo.js"></script>
    <script src="/js/appear.js"></script>
    <script src="/js/tipso.js"></script>
    <script src="/js/lightpick.js"></script>
    <script src="/js/main-min.js"></script>
    @yield('scripts')
</body>
